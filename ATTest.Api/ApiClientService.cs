﻿using ATTest.Api.Configuration;
using ATTest.Api.Extensions;
using RestSharp.Authenticators;
using RestSharp;
using System.Diagnostics;
using ATTest.Api.Interfaces;

namespace ATTest.Api
{
    public class ApiClientService
    {
        private readonly ExecutionProvider _executionProvider;
        private readonly ApiSettings _apiSettings = ConfigurationService.GetSection<ApiSettings>();
        //private RestClient WrappedClient;

        public int MaxRetryAttempts { get; set; }
        public string BaseUrl
        {
            get => WrappedClient.BaseUrl.AbsoluteUri;
            set => WrappedClient.BaseUrl = new Uri(value);
        }
        public TimeSpan PauseBetweenFailures { get; set; }
        public IRestClient WrappedClient { get; set; }

        public ApiClientService()
        {
            _executionProvider = new ExecutionProvider();
            InitializeExecutionExtensions(_executionProvider);

            if (WrappedClient == null)
            {
                WrappedClient = new RestClient();
            }
            WrappedClient.AddHandler("application/json", () => NewtonsoftJsonSerializer.Default);
            WrappedClient.AddHandler("text/json", () => NewtonsoftJsonSerializer.Default);
            WrappedClient.AddHandler("text/x-json", () => NewtonsoftJsonSerializer.Default);
            WrappedClient.AddHandler("text/javascript", () => NewtonsoftJsonSerializer.Default);
            WrappedClient.AddHandler("*+json", () => NewtonsoftJsonSerializer.Default);

            _executionProvider.OnClientInitialized(WrappedClient);
            var authenticator = ServicesCollection.Current.Resolve<IAuthenticator>();
            if (authenticator != null)
            {
                WrappedClient.Authenticator = authenticator;
            }

            WrappedClient.FollowRedirects = true;

            if (_apiSettings != null)
            {
                MaxRetryAttempts = _apiSettings.MaxRetryAttempts;
                PauseBetweenFailures = ATTest.Api.Utilities.TimeSpanConverter.Convert(_apiSettings.PauseBetweenFailures, _apiSettings.TimeUnit);

                int timeoutSeconds = _apiSettings.ClientTimeoutSeconds;
                //Policy.Timeout(timeoutSeconds, onTimeout: (context, timespan, task) =>
                //{
                //    task.ContinueWith(t =>
                //    {
                //        if (t.IsFaulted)
                //        {
                //            _executionProvider.OnRequestTimeout(WrappedClient);
                //        }
                //    });
                //});
            }
            else
            {
                throw new ArgumentNullException("The API section in testFrameworkSettings.json is missing.");
            }
        }

        public ApiClientService(string BaseUrl) : this()
        {
            WrappedClient.BaseUrl = new Uri(BaseUrl);
        }

        #region Process Sync
        public IDataResponse Execute(IRestRequest request, Method method = Method.GET) => ExecuteMeasuredRequest(request, (RestSharp.Method)method);

        public IDataResponse Get(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.GET);

        public IDataResponse Put(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.PUT);

        public IDataResponse Post(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.POST);

        public IDataResponse Delete(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.DELETE);

        public IDataResponse Copy(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.COPY);

        public IDataResponse Head(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.HEAD);

        public IDataResponse Merge(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.MERGE);

        public IDataResponse<TReturnType> Get<TReturnType>(IRestRequest request, bool shouldReturnResponseOnFailure = false)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.GET, shouldReturnResponseOnFailure);

        public IDataResponse<TReturnType> Post<TReturnType>(IRestRequest request, bool shouldReturnResponseOnFailure = false)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.POST, shouldReturnResponseOnFailure);

        public IDataResponse<TReturnType> Put<TReturnType>(IRestRequest request, bool shouldReturnResponseOnFailure = false)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.PUT, shouldReturnResponseOnFailure);

        public IDataResponse<TReturnType> Patch<TReturnType>(IRestRequest request)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.PATCH);

        public IDataResponse<TReturnType> Delete<TReturnType>(IRestRequest request, bool shouldReturnResponseOnFailure = false)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.DELETE, shouldReturnResponseOnFailure);

        public IDataResponse<TReturnType> Copy<TReturnType>(IRestRequest request)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.COPY);

        public IDataResponse<TReturnType> Head<TReturnType>(IRestRequest request)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.HEAD);

        public IDataResponse Options(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.OPTIONS);

        public IDataResponse<TReturnType> Options<TReturnType>(IRestRequest request)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.OPTIONS);

        public IDataResponse<TReturnType> Merge<TReturnType>(IRestRequest request)
            where TReturnType : new() => ExecuteMeasuredRequest<TReturnType>(request, (RestSharp.Method)Method.MERGE);

        public IDataResponse Patch(IRestRequest request) => ExecuteMeasuredRequest(request, (RestSharp.Method)Method.PATCH);
        #endregion

        #region Process Async
        public async Task<IDataResponse> ExecuteAsync(IRestRequest request, Method method = Method.GET, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)method, cancellationTokenSource).ConfigureAwait(false);
        public async Task<IDataResponse<TReturnType>> ExecuteAsync<TReturnType>(IRestRequest request, Method method = Method.GET, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)method, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> GetAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.GET, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> GetAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.GET, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> PutAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.PUT, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> PutAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.PUT, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> PostAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.POST, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> PostAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.POST, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> DeleteAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.DELETE, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> DeleteAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.DELETE, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> CopyAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.COPY, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> CopyAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, RestSharp.Method.COPY, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> HeadAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.HEAD, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> HeadAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.HEAD, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> MergeAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.MERGE, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> MergeAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.MERGE, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> OptionsAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.OPTIONS, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> OptionsAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.OPTIONS, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse> PatchAsync(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            => await ExecuteMeasuredRequestAsync(request, (RestSharp.Method)Method.PATCH, cancellationTokenSource).ConfigureAwait(false);

        public async Task<IDataResponse<TReturnType>> PatchAsync<TReturnType>(IRestRequest request, CancellationTokenSource cancellationTokenSource = null)
            where TReturnType : new() => await ExecuteMeasuredRequestAsync<TReturnType>(request, (RestSharp.Method)Method.PATCH, cancellationTokenSource).ConfigureAwait(false);
        #endregion

        #region Private
        private async Task<IDataResponse<TReturnType>> ExecuteMeasuredRequestAsync<TReturnType> (
            IRestRequest request,
            RestSharp.Method method,
            CancellationTokenSource cancellationTokenSource = null
        ) where TReturnType : new()
        {
            if (cancellationTokenSource == null)
            {
                cancellationTokenSource = new CancellationTokenSource();
            }

            var dataResponse = default(DataResponse<TReturnType>);
            //var taskCompletionSource = new TaskCompletionSource<IDataResponse<TReturnType>>();

            //(async () => {
            //    dataResponse = (DataResponse<TReturnType>?)await WrappedClient.ExecuteTaskAsync<IDataResponse<TReturnType>>(request);
            //});
            
            try
            {
                var watch = Stopwatch.StartNew();

                request.Method = method;
                SetJsonContent(request);

                _executionProvider.OnMakingRequest(request, request.Resource);

                var response = await WrappedClient.ExecuteAsync<TReturnType>(request, cancellationTokenSource.Token).ConfigureAwait(true);

                _executionProvider.OnRequestMade(response, request.Resource);

                watch.Stop();
                dataResponse = new DataResponse<TReturnType>(response, watch.Elapsed);

                if (!dataResponse.IsSuccessful)
                {
                    _executionProvider.OnRequestFailed(response, request.Resource);
                    throw new NotSuccessfulRequestException();
                }
            }
            catch (Exception ex)
            {
#pragma warning disable CA2200 // Rethrow to preserve stack details
                throw ex;
            }

            return dataResponse;
        }

        private async Task<IDataResponse> ExecuteMeasuredRequestAsync(
            IRestRequest request,
            RestSharp.Method method,
            CancellationTokenSource cancellationTokenSource = null
        )
        {
            if (cancellationTokenSource == null)
            {
                cancellationTokenSource = new CancellationTokenSource();
            }

            var dataResponse = default(DataResponse);

            try
            {
                var watch = Stopwatch.StartNew();

                request.Method = method;
                SetJsonContent(request);

                _executionProvider.OnMakingRequest(request, request.Resource);

                var response = await WrappedClient.ExecuteAsync(request, cancellationTokenSource.Token).ConfigureAwait(false);

                _executionProvider.OnRequestMade(response, request.Resource);

                watch.Stop();
                dataResponse = new DataResponse(response, watch.Elapsed);

                if (!dataResponse.IsSuccessful)
                {
                    throw new NotSuccessfulRequestException();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dataResponse;
        }

        private IDataResponse<TReturnType> ExecuteMeasuredRequest<TReturnType>(
            IRestRequest request,
            RestSharp.Method method,
            bool shouldReturnReponseOnFailure = false
        ) where TReturnType : new()
        {
            var dataResponse = default(DataResponse<TReturnType>);

            try
            {
                var watch = Stopwatch.StartNew();

                request.Method = method;
                SetJsonContent(request);

                _executionProvider.OnMakingRequest(request, request.Resource);

                var response = WrappedClient.Execute<TReturnType>(request);

                _executionProvider.OnRequestMade(response, request.Resource);

                watch.Stop();
                dataResponse = new DataResponse<TReturnType>(response, watch.Elapsed);

                if (!dataResponse.IsSuccessful && !shouldReturnReponseOnFailure)
                {
                    throw new NotSuccessfulRequestException($"Failed on URL= {dataResponse.ResponseUri} {Environment.NewLine} {dataResponse.StatusCode} {Environment.NewLine} {dataResponse.Content}. Elapsed Time: {dataResponse.ExecutionTime.ToString()}");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dataResponse;
        }

        private IDataResponse ExecuteMeasuredRequest(IRestRequest request, RestSharp.Method method)
        {
            var dataResponse = default(DataResponse);

            try
            {
                var watch = Stopwatch.StartNew();

                request.Method = method;
                SetJsonContent(request);

                _executionProvider.OnMakingRequest(request, request.Resource);

                var response = WrappedClient.Execute(request);

                _executionProvider.OnRequestMade(response, request.Resource);

                watch.Stop();
                dataResponse = new DataResponse(response, watch.Elapsed);

                if (!dataResponse.IsSuccessful)
                {
                    throw new NotSuccessfulRequestException($"Failed on URL= {dataResponse.ResponseUri} {Environment.NewLine} {dataResponse.StatusCode} {Environment.NewLine} {dataResponse.Content}. Elapsed Time: {dataResponse.ExecutionTime.ToString()}");
                }
            }
            catch (Exception ex)
            {
                throw new NotSuccessfulRequestException($"Error message: {ex.Message}");
            }

            return dataResponse;
        }

        private void InitializeExecutionExtensions(ExecutionProvider executionProvider)
        {
            var observers = ServicesCollection.Current.ResolveAll<ApiClientExecutionPlugin>();
            foreach (var observer in observers)
            {
                observer.Subscribe(executionProvider);
            }
        }

        private void SetJsonContent(IRestRequest request, object obj = null)
        {
            request.JsonSerializer = NewtonsoftJsonSerializer.Default;
            if (obj != null)
            {
                request.AddJsonBody(obj);
            }
        }
        #endregion
    }
}
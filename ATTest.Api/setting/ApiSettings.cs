﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATTest.Api.Configuration
{
    public class ApiSettings
    {
        public string BaseUrl { get; set; }
        public int ClientTimeoutSeconds { get; set; }
        public int MaxRetryAttempts { get; set; }
        public int PauseBetweenFailures { get; set; }
        public TimeUnit TimeUnit { get; set; }
        public string UserAgent { get; set; } = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36";
    }
}

﻿using ATTest.Api.Configuration;

namespace ATTest
{
    public static class ConfigurationSettingsExtensions
    {
        public static ApiSettings GetApiSettings(this ConfigurationService service)
        {
            return ConfigurationService.GetSection<ApiSettings>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ATTest.Core
{
    public class HttpRequestHeaderModel
    {
        public string Authorization { get; set; }
        public string RequestVerificationToken { get; set; }
        public IReadOnlyCollection<Cookie> Cookies;

        public HttpRequestHeaderModel(IReadOnlyCollection<Cookie> cookies, string requestVerificationToke)
        {
            Cookies = cookies;
            RequestVerificationToken = requestVerificationToke;
        }

        public HttpRequestHeaderModel(string authorization)
        {
            Authorization = authorization;
        }
    }
}

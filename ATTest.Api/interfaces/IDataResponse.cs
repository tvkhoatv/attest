﻿using System;
using RestSharp;

namespace ATTest.Api.Interfaces
{
    public interface IDataResponse : IRestResponse
    {
        TimeSpan ExecutionTime { get; set; }
    }
}

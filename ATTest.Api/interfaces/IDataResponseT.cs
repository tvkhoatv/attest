﻿using RestSharp;

namespace ATTest.Api.Interfaces
{
    public interface IDataResponse<TReturnType> : IRestResponse<TReturnType> where TReturnType : new()
    {}
}

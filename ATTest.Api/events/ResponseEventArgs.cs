﻿using RestSharp;

namespace ATTest.Api.Events
{
    public class ResponseEventArgs
    {
        public ResponseEventArgs(IRestResponse response, string requestUri)
        {
            Response = response;
            RequestUri = requestUri;
        }

        public IRestResponse Response { get; }
        public string RequestUri { get; }
    }
}

﻿using ATTest.Api.Configuration;
using ATTest.Api.Interfaces;
using ATTest.Plugins;
using RestSharp;
using System.Net;

namespace ATTest.Api;

public class Rest
{
    private readonly ApiClientService _apiClientService;
    public string baseAddress;

    public Rest()
    {
        _apiClientService = GetApiClientDefault();
    }

    public Rest(string baseUrl) : this()
    {
        baseAddress = baseUrl;
    }

    public static Rest Instance => new Lazy<Rest>(() => new Rest()).Value;
    public static Rest Using(string baseUrl) => new Rest(baseUrl);

    public ApiClientService ApiClient
    {
        get => _apiClientService;
        init
        {
            _apiClientService = GetApiClientDefault();
        }
    }

    public bool ShouldReuseRestClient { get; set; } = true;

    //public void AddApiClientExecutionPlugin<TExecutionExtension>() where TExecutionExtension : ApiClientExecutionPlugin
    //{
    //    ServicesCollection.Current.RegisterType<ApiClientExecutionPlugin, TExecutionExtension>(Guid.NewGuid().ToString());
    //}

    public void AddPlugin<TExecutionExtension>() where TExecutionExtension : Plugin
    {
        ServicesCollection.Current.RegisterType<Plugin, TExecutionExtension>(Guid.NewGuid().ToString());
    }

    public ApiClientService GetApiClientDefault()
    {
        bool isClientRegistered = ServicesCollection.Current.IsRegistered<ApiClientService>();
        var client = ServicesCollection.Current.Resolve<ApiClientService>();

        if (!isClientRegistered || client == null)
        {
            client = new ApiClientService();
        }

        return client;
    }

    public ApiClientService GetApiClientService(
        string url = null,
        bool sharedCookies = false,
        int maxRetryAttempts = 1,
        int pauseBetweenFailures = 1,
        TimeUnit timeUnit = TimeUnit.Seconds
    )
    {
        if (!ShouldReuseRestClient)
        {
            ServicesCollection.Current.UnregisterSingleInstance<ApiClientService>();
        }

        bool isClientRegistered = ServicesCollection.Current.IsRegistered<ApiClientService>();
        var client = ServicesCollection.Current.Resolve<ApiClientService>();

        if (!isClientRegistered || client == null)
        {
            client = new ApiClientService();
            if (string.IsNullOrEmpty(url))
            {
                var apiSettingsConfig = ConfigurationService.GetSection<ApiSettings>();
                if (apiSettingsConfig == null)
                {
                    throw new SettingsNotFoundException("apiSettings");
                }

                client.WrappedClient.BaseUrl = new Uri(apiSettingsConfig.BaseUrl);
            }
            else
            {
                client.WrappedClient.BaseUrl = new Uri(url);
            }

            if (sharedCookies)
            {
                client.WrappedClient.CookieContainer = new System.Net.CookieContainer();
            }

            client.PauseBetweenFailures = Utilities.TimeSpanConverter.Convert(pauseBetweenFailures, timeUnit);
            client.MaxRetryAttempts = maxRetryAttempts;

            ServicesCollection.Current.RegisterInstance(client);
        }

        return client;
    }

    public static object GetReponse(
        string requestUrl,
        object requestBody = null,
        Method method = Method.GET,
        List<KeyValuePair<string, object>> additionHeaders = null,
        bool fromSubDriver = false
    )
    {
        ApiClientService client = Rest.Instance.GetApiClientService(requestUrl);

        var request = new RestRequest(client.BaseUrl);
        request.RequestFormat = DataFormat.Json;

        if (requestBody != null)
        {
            request.AddBody(requestBody);
        }

        var response =  Rest.Instance.SendRequest(request, client, method);
        return response;
    }

    private object SendRequest(RestRequest request, ApiClientService client, Method method)
    {
        object result;
        ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        {
            result = client.Execute(request, method);
        }
        return result;
    }

    private static async Task<object> SendRequestAsync(RestRequest request, ApiClientService client, Method method)
    {
        IDataResponse result;
        ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        {
            //result = await Task.Run(() => client.GetAsync<object>(request));
            result = (IDataResponse)await client.GetAsync<object>(request);

        }
        return result;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATTest.Api
{
    public enum TimeUnit : byte
    {
        Milliseconds = 0,
        Seconds = 1,
        Minutes = 2,
    }
}

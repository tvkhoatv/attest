﻿namespace ATTest.Api
{
    public enum Method
    {
        POST = RestSharp.Method.POST,
        PUT = RestSharp.Method.PUT,
        DELETE = RestSharp.Method.DELETE,
        PATCH = RestSharp.Method.PATCH,
        GET = RestSharp.Method.GET,
        COPY = RestSharp.Method.COPY,
        OPTIONS = RestSharp.Method.OPTIONS,
        HEAD = RestSharp.Method.HEAD,
        MERGE = RestSharp.Method.MERGE,
    }
}

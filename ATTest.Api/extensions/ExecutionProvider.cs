﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ATTest.Api.Events;
using ATTest.Api.Extensions;
using RestSharp;

namespace ATTest.Api.Extensions
{
    public class ExecutionProvider : IExecutionProvider
    {
        public event EventHandler<ClientEventArgs> OnClientInitializedEvent;

        public event EventHandler<ClientEventArgs> OnRequestTimeoutEvent;

        public event EventHandler<RequestEventArgs> OnMakingRequestEvent;

        public event EventHandler<ResponseEventArgs> OnRequestMadeEvent;

        public event EventHandler<ResponseEventArgs> OnRequestFailedEvent;
    
        public void OnClientInitialized(IRestClient client) => OnClientInitializedEvent?.Invoke(this, new ClientEventArgs(client));
        public void OnRequestTimeout(IRestClient client) => OnRequestTimeoutEvent?.Invoke(this, new ClientEventArgs(client));
        public void OnMakingRequest(IRestRequest request, string requestUri) => OnMakingRequestEvent?.Invoke(this, new RequestEventArgs(request, requestUri));
        public void OnRequestMade(IRestResponse response, string requestUri) => OnRequestMadeEvent?.Invoke(this, new ResponseEventArgs(response, requestUri));
        public void OnRequestFailed(IRestResponse response, string requestUri) => OnRequestFailedEvent?.Invoke(this, new ResponseEventArgs(response, requestUri));
    }
}

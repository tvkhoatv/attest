﻿using ATTest.Api.Events;

namespace ATTest.Api.Extensions
{
    public interface IExecutionProvider
    {
        event EventHandler<ClientEventArgs> OnClientInitializedEvent;
        event EventHandler<ClientEventArgs> OnRequestTimeoutEvent;
        event EventHandler<RequestEventArgs> OnMakingRequestEvent;
        event EventHandler<ResponseEventArgs> OnRequestMadeEvent;
        event EventHandler<ResponseEventArgs> OnRequestFailedEvent;
    }
}

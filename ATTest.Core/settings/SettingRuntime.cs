﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ATTest.Core.Settings
{
    public sealed class SettingRuntime
    {
        public static TestContext? Context { get; set; }

        public SettingRuntime() { }

        public SettingRuntime(TestContext context) : this()
        {
            Context = context;
        }

        public static SettingRuntime Instance => new Lazy<SettingRuntime>(() => new SettingRuntime()).Value;

        public static dynamic GetProperty(string property)
        {
            return Context?.Properties[property] ?? null;
        }
    }
}

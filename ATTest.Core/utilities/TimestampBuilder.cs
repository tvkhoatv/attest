﻿using System;
using System.Linq;

namespace ATTest.Utilities
{

    public class TimestampBuilder
    {
        public static string BuildUniqueText(string text)
        {
            var newTimestamp = GenerateUniqueText();
            var result = string.Concat(text, newTimestamp);
            return result;
        }

        public static string GenerateUniqueText()
        {
            var newTimestamp = DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss-ffff");
            return newTimestamp;
        }

        public static string GenerateUniqueUrl()
        {
            var newTimestamp = DateTime.Now.ToString("MMMMddyyyyhhmmss");
            return newTimestamp;
        }
        public static string GenerateUniqueTextMonthNameOneWord()
        {
            var newTimestamp = DateTime.Now.ToString("MMMMddyyyyhhmmss");
            return newTimestamp;
        }
    }
}
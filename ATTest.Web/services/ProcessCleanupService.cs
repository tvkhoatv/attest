﻿using System.Diagnostics;
using System.Runtime.InteropServices;
using ATTest.Web;

namespace ATTest.Utilities
{
    public static class ProcessCleanupService
    {
        private static readonly WebSettings ProcessCleanupSettings = ConfigurationService.GetSection<WebSettings>();
        private static readonly bool IsParallelExecutionEnabled = ProcessCleanupSettings?.IsParallelExecutionEnabled ?? false;

        public static void KillPreviousDriversAndBrowsersOsAgnostic(DateTime? executionStartDate)
        {
            var browsersToCheck = new List<string>
            {
                "opera",
                "chrome",
                "firefox",
                "edge",
                "iexplore",
                "safari",
            };

            var driversToCheck = new List<string>
            {
                "operadriver",
                "chromedriver",
                "iedriverserver",
                "geckodriver",
                "msedgedriver",
            };

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                KillAllProcesses(driversToCheck);
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                var driversAndBrowsersТoCheck = driversToCheck.Union(browsersToCheck).ToList();

                KillProcessStartedAfterTime(driversAndBrowsersТoCheck, executionStartDate);
            }
        }

        public static void KillAllDriversAndChildProcessesWindows()
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return;
            }

            if (IsParallelExecutionEnabled)
            {
                return;
            }

            var driversToCheck = new List<string>
            {
                "operadriver",
                "chromedriver",
                "iedriverserver",
                "geckodriver",
                "msedgedriver",
            };

            KillAllProcesses(driversToCheck);
        }

        private static void KillAllProcesses(List<string> processesToKill)
        {
            var processes = Process.GetProcesses().Where(p => processesToKill.Any(x => p.ProcessName.ToLower().Contains(x)));

            foreach (var process in processes)
            {
                try
                {
                    var children = process.GetChildProcesses();
                    foreach (var child in children)
                    {
                        child.Kill();
                    }

                    process.Kill();
                }
                catch (Exception e)
                {
                    e.PrintStackTrace();
                }
            }
        }

        private static void KillProcessStartedAfterTime(List<string> processesToKill, DateTime? executionStartDate)
        {
            var processes = Process.GetProcesses().Where(p => processesToKill.Any(x => p.ProcessName.ToLower().Contains(x)));
            foreach (var process in processes)
            {
                try
                {
                    Debug.WriteLine(process.ProcessName);
                    if (process.StartTime > executionStartDate)
                    {
                        process.Kill();
                    }
                }
                catch (Exception e)
                {
                    e.PrintStackTrace();
                }
            }
        }
    }
}

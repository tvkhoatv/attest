﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace ATTest.Web
{
    public class WebApiService
    {
        #region Declare fields
        private const string UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36";
        public string baseAddress;
        public HttpContent content;
        private HttpResponseMessage response;
        private string result;

        public static Dictionary<CookieType, HttpRequestHeaderModel> RequestHeaders = new Dictionary<CookieType, HttpRequestHeaderModel>()
        {
            {CookieType.Web, null},
            {CookieType.Portal, null},
            {CookieType.Mobile, null },
            {CookieType.SubWeb, null }
        };
        #endregion

        #region Constructor
        public WebApiService()
        {
            //baseAddress = $"{AppConfigFactory.Instance.Url}/";
        }
        public WebApiService(string baseUrl)
        {
            baseAddress = baseUrl;
        }

        public static WebApiService Instance => new Lazy<WebApiService>(() => new WebApiService()).Value;
        public static WebApiService Using(string baseUrl) => new WebApiService(baseUrl);
        #endregion

        #region Run Async
        public async Task<string> GetResponseAsync(
            string requestUrl,
            object requestBody = null, 
            HttpVerbs method = HttpVerbs.Post,
            List<KeyValuePair <string, object>> additionHeaders = null,
            bool fromSubDriver = false
        )
        {
            response = await SendRequestAsync(requestUrl, requestBody, method, additionHeaders, fromSubDriver);
            response.EnsureSuccessStatusCode();
            result = await response.Content.ReadAsStringAsync();
            return result;
        }
        #endregion

        #region Run Sync

        #endregion

        #region Private

        #endregion
    }
}

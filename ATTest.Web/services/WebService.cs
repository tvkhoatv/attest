﻿using System;
using OpenQA.Selenium;

namespace ATTest.Web;

public abstract class WebService
{
    protected WebService(IWebDriver wrappedDriver)
    {
        WrappedDriver = wrappedDriver;
    }

    public IWebDriver WrappedDriver { get; }
}

﻿using System;
using ATTest.Web;
using ATTest.Web.Services;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace ATTest.Web;

public class TestExecutionEngine
{
    public void StartBrowser(BrowserConfiguration browserConfiguration, ServicesCollection childContainer)
    {
        try
        {
            var wrappedWebDriver = WrappedWebDriverCreateService.Create(browserConfiguration);

            childContainer.RegisterInstance<IWebDriver>(wrappedWebDriver);
            childContainer.RegisterInstance(((WebDriver)wrappedWebDriver).SessionId.ToString(), "SessionId");
            childContainer.RegisterInstance(ConfigurationService.GetSection<WebSettings>().ExecutionSettings.Url, "GridUri");
            childContainer.RegisterInstance<IWebDriverElementFinderService>(new NativeElementFinderService(wrappedWebDriver));
            childContainer.RegisterNull<int?>();
            childContainer.RegisterNull<IWebElement>();
            IsBrowserStartedCorrectly = true;
        }
        catch (Exception ex)
        {
            DebugInfo.PrintStackTrace(ex);
            IsBrowserStartedCorrectly = false;
            throw;
        }
    }

    public bool IsBrowserStartedCorrectly { get; set; }

    public void Dispose(ServicesCollection container)
    {
        var webDriver = container.Resolve<IWebDriver>();
        DisposeDriverService.Dispose(webDriver, container);
    }

    public void DisposeAll()
    {
        foreach (var childContainer in ServicesCollection.Current.GetChildServicesCollections())
        {
            var driver = childContainer.Resolve<IWebDriver>();
            DisposeDriverService.Dispose(driver, childContainer);
        }
    }
}
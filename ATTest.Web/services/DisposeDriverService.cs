﻿using OpenQA.Selenium;
using System;
using ATTest.Utilities;

namespace ATTest.Web.Services
{
    public static class DisposeDriverService
    {
        public static DateTime? TestRunStartTime { get; set; }

        public static void Dispose()
        {
            try
            {
                var driver = ServicesCollection.Current.Resolve<IWebDriver>();
                driver?.Close();
                driver?.Quit();
                driver?.Dispose();
                ServicesCollection.Current?.UnregisterSingleInstance<IWebDriver>();
            }
            catch (Exception ex)
            {
                DebugInfo.PrintStackTrace(ex);
            }

            ProcessCleanupService.KillPreviousDriversAndBrowsersOsAgnostic(TestRunStartTime);
        }

        public static void Dispose(IWebDriver webDriver, ServicesCollection container)
        {
            try
            {
                webDriver?.Close();
                webDriver?.Quit();
                webDriver?.Dispose();
                container?.UnregisterSingleInstance<IWebDriver>();
            }
            catch (Exception ex)
            {
                DebugInfo.PrintStackTrace(ex);
            }

            ProcessCleanupService.KillPreviousDriversAndBrowsersOsAgnostic(TestRunStartTime);
        }

        public static void DisposeAll()
        {
            foreach (var childContainer in ServicesCollection.Main.GetChildServicesCollections())
            {
                try
                {
                    var driver = childContainer.Resolve<IWebDriver>();
                    driver?.Close();
                    driver?.Quit();
                    driver?.Dispose();
                    childContainer?.UnregisterSingleInstance<IWebDriver>();
                }
                catch (Exception ex)
                {
                    DebugInfo.PrintStackTrace(ex);
                }
            }
        }
    }
}

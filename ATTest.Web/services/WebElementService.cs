﻿using ATTest.Web.Locators;
using OpenQA.Selenium;

namespace ATTest.Web
{
    public class WebElementService: WebService
    {
        public WebElementService(IWebDriver wrappedDriver) : base(wrappedDriver)
        {
        }

        #region Get IWebDriver
        public IWebElement FindByIdEndingWith(string idEnding, bool shouldCacheElement = false)
            => (new FindIdEndingWithStrategy(idEnding)).Find(WrappedDriver);

        public IWebElement FindByTag(string tag, bool shouldCacheElement = false)
            => (new FindTagStrategy(tag)).Find(WrappedDriver);

        public IWebElement FindById(string id, bool shouldCacheElement = false)
            => (new FindIdStrategy(id)).Find(WrappedDriver);

        public IWebElement FindByIdContaining(string idContaining, bool shouldCacheElement = false)
            => (new FindIdContainingStrategy(idContaining)).Find(WrappedDriver);

        public IWebElement FindByValueContaining(string valueEnding, bool shouldCacheElement = false)
            => (new FindValueContainingStrategy(valueEnding)).Find(WrappedDriver);

        public IWebElement FindByXpath(string xpath, bool shouldCacheElement = false)
            => (new FindXpathStrategy(xpath)).Find(WrappedDriver);

        public IWebElement FindByLinkText(string linkText, bool shouldCacheElement = false)
            => (new FindLinkTextStrategy(linkText)).Find(WrappedDriver);

        public IWebElement FindByLinkTextContaining(string linkTextContaining, bool shouldCacheElement = false)
            => (new FindLinkTextContainsStrategy(linkTextContaining)).Find(WrappedDriver);

        public IWebElement FindByClass(string cssClass, bool shouldCacheElement = false)
            => (new FindClassStrategy(cssClass)).Find(WrappedDriver);

        public IWebElement FindByCss(string cssClass, bool shouldCacheElement = false)
            => (new FindCssStrategy(cssClass)).Find(WrappedDriver);

        public IWebElement FindByClassContaining(string cssClassContaining, bool shouldCacheElement = false)
            => (new FindClassContainingStrategy(cssClassContaining)).Find(WrappedDriver);

        public IWebElement FindByInnerTextContaining(string innerText, bool shouldCacheElement = false)
            => (new FindInnerTextContainsStrategy(innerText)).Find(WrappedDriver);

        public IWebElement FindByNameEndingWith(string name, bool shouldCacheElement = false)
            => (new FindNameEndingWithStrategy(name)).Find(WrappedDriver);

        public IWebElement FindByAttributesContaining(string attributeName, string value, bool shouldCacheElement = false)
            => (Find.By.AttributeContaining(attributeName, value)).Find(WrappedDriver);
        #endregion
    }
}

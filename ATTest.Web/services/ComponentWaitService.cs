﻿using ATTest.Web.Events;
using ATTest.Web.Untils;

namespace ATTest.Web.Waits;

public class ComponentWaitService
{
    public static event EventHandler<ElementNotFulfillingWaitConditionEventArgs> OnElementNotFulfillingWaitConditionEvent;

    public void Wait<TUntil, TComponent>(TComponent element, TUntil until)
        where TUntil : WaitStrategy
        where TComponent : Component
    {
        try
        {
            if (element.ParentWrappedElement == null)
            {
                WaitInternal(element.By, until);
            }
            else
            {
                var elementRepository = new ComponentRepository();
                Component parenTComponent = elementRepository.CreateComponentThatIsFound<Component>(element.By, element.ParentWrappedElement, true);
                WaitInternal(element.By, until, parenTComponent);
            }
        }
        catch (Exception ex)
        {
            OnElementNotFulfillingWaitConditionEvent?.Invoke(this, new ElementNotFulfillingWaitConditionEventArgs(ex));
            throw;
        }
    }

    internal void WaitInternal<TUntil, TBy>(TBy by, TUntil until)
        where TUntil : WaitStrategy
        where TBy : FindStrategy => until?.WaitUntil(@by);

    internal void WaitInternal<TUntil, TBy>(TBy by, TUntil until, Component parent)
        where TUntil : WaitStrategy
        where TBy : FindStrategy => until?.WaitUntil(@by, parent);
}

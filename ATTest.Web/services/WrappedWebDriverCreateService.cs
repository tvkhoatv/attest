﻿using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using System.Net.Sockets;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WebDriverManager.DriverConfigs.Impl;
using WebDriverManager.Helpers;
using WebDriverManager;
using ATTest.Core;
using ATTest.Core.Settings;
using ATTest.Core.Utilities;
using ATTest.Utilities;
using ATTest.Web.Services;
using ATTest.Web.Enums;
using System.Drawing;
using ATTest.Web.Proxy;

namespace ATTest.Web
{
    public static class WrappedWebDriverCreateService
    {
        private static readonly string _driverExecutablePath = ExecutionDirectoryResolver.GetDriverExecutablePath();

        private static ProxyService _proxyService;
        public static BrowserConfiguration BrowserConfiguration { get; set; }
        public static int Port { get; set; }
        public static int DebuggerPort { get; set; }

        public static IWebDriver Create(BrowserConfiguration executionConfiguration)
        {
            ProcessCleanupService.KillAllDriversAndChildProcessesWindows();

            DisposeDriverService.TestRunStartTime = DateTime.Now;

            BrowserConfiguration = executionConfiguration;
            _proxyService = ServicesCollection.Current.Resolve<ProxyService>();

            var wrappedWebDriver = default(IWebDriver);
            var webDriverProxy = new OpenQA.Selenium.Proxy
            {
                HttpProxy = $"http://127.0.0.1:{_proxyService.Port}",
                SslProxy = $"http://127.0.0.1:{_proxyService.Port}",
            };

            switch (executionConfiguration.ExecutionType)
            {
                case ExecutionType.Regular:
                    wrappedWebDriver = InitializeDriverRegularMode(executionConfiguration, webDriverProxy);
                    break;

                case ExecutionType.Grid:
                case ExecutionType.SauceLabs:
                    var gridUrl = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.Url;
                    if (gridUrl == null || !Uri.IsWellFormedUriString(gridUrl.ToString(), UriKind.Absolute))
                    {
                        throw new ArgumentException("To execute your tests in WebDriver Grid mode you need to set the gridUri in the browserSettings file.");
                    }

                    DebuggerPort = GetFreeTcpPort();

                    if (executionConfiguration.IsLighthouseEnabled && (executionConfiguration.BrowserType.Equals(BrowserType.Chrome) || executionConfiguration.BrowserType.Equals(BrowserType.ChromeHeadless)))
                    {
                        executionConfiguration.DriverOptions.AddArgument("--remote-debugging-address=0.0.0.0");
                        executionConfiguration.DriverOptions.AddArgument($"--remote-debugging-port={DebuggerPort}");
                    }

                    Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                    var options = executionConfiguration.DriverOptions;
                    if (!executionConfiguration.BrowserType.Equals(BrowserType.Safari) && !executionConfiguration.BrowserType.Equals(BrowserType.Firefox))
                    {
                        options.AddLocalStatePreference("browser", new { enabled_labs_experiments = GetExperimentalOptions() });
                    }
                    wrappedWebDriver = new RemoteWebDriver(new Uri(gridUrl), options.ToCapabilities(), TimeSpan.FromSeconds(180));

                    IAllowsFileDetection allowsDetection = wrappedWebDriver as IAllowsFileDetection;
                    if (allowsDetection != null)
                    {
                        allowsDetection.FileDetector = new LocalFileDetector();
                    }

                    break;
            }

            var gridPageLoadTimeout = ConfigurationService.GetSection<WebSettings>().TimeoutSettings.PageLoadTimeout;
            wrappedWebDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(gridPageLoadTimeout);
            var gridScriptTimeout = ConfigurationService.GetSection<WebSettings>().TimeoutSettings.ScriptTimeout;
            wrappedWebDriver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(gridScriptTimeout);

            if (executionConfiguration.BrowserType != BrowserType.Edge)
            {
                FixDriverCommandExecutionDelay(wrappedWebDriver);

                ////DriverCommandExecutionService commandExecutionService = new DriverCommandExecutionService((RemoteWebDriver)wrappedWebDriver);
                ////commandExecutionService.InitializeSendCommand((RemoteWebDriver)wrappedWebDriver);
            }

            ChangeWindowSize(executionConfiguration.Size, wrappedWebDriver);

            return wrappedWebDriver;
        }

        private static void FixDriverCommandExecutionDelay(IWebDriver driver)
        {
            try
            {
                PropertyInfo commandExecutorProperty = GetPropertyWithThrowOnError(typeof(WebDriver), "CommandExecutor", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetProperty);
                ICommandExecutor commandExecutor = (ICommandExecutor)commandExecutorProperty.GetValue(driver);

                FieldInfo GetRemoteServerUriField(ICommandExecutor executor)
                {
                    return executor.GetType().GetField("remoteServerUri", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField);
                }

                FieldInfo remoteServerUriField = GetRemoteServerUriField(commandExecutor);

                if (remoteServerUriField == null)
                {
                    FieldInfo internalExecutorField = commandExecutor.GetType().GetField("internalExecutor", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
                    commandExecutor = (ICommandExecutor)internalExecutorField.GetValue(commandExecutor);

                    ServicesCollection.Current.RegisterInstance(commandExecutor);

                    remoteServerUriField = GetRemoteServerUriField(commandExecutor);
                }

                if (remoteServerUriField != null)
                {
                    string remoteServerUri = remoteServerUriField.GetValue(commandExecutor).ToString();

                    string localhostUriPrefix = "http://localhost";

                    if (remoteServerUri.StartsWith(localhostUriPrefix, StringComparison.Ordinal))
                    {
                        remoteServerUri = remoteServerUri.Replace(localhostUriPrefix, "http://127.0.0.1");

                        remoteServerUriField.SetValue(commandExecutor, new Uri(remoteServerUri));
                    }
                }
            }
            catch (Exception e)
            {
                // Failed to apply fix of command execution delay.
                e.PrintStackTrace();
            }
        }

        internal static PropertyInfo GetPropertyWithThrowOnError(Type type, string name, BindingFlags bindingFlags = BindingFlags.Default)
        {
            PropertyInfo property = bindingFlags == BindingFlags.Default ? type.GetProperty(name) : type.GetProperty(name, bindingFlags);

            if (property == null)
            {
                throw new MissingMemberException(type.FullName, name);
            }

            return property;
        }

        private static IWebDriver InitializeDriverRegularMode(BrowserConfiguration executionConfiguration, OpenQA.Selenium.Proxy webDriverProxy)
        {
            IWebDriver wrappedWebDriver;
            switch (executionConfiguration.BrowserType)
            {
                case BrowserType.Chrome:
                    wrappedWebDriver = new ChromeBrowser(executionConfiguration).Init();
                    wrappedWebDriver.Manage().Window.Maximize();
                    break;

                case BrowserType.ChromeHeadless:
                    wrappedWebDriver = new ChromeBrowserHeadless(executionConfiguration).Init();
                    break;

                case BrowserType.Firefox:
                    wrappedWebDriver = new FirefoxBrowser(executionConfiguration).Init();
                    break;

                case BrowserType.FirefoxHeadless:
                    wrappedWebDriver = new FirefoxBrowserHeadless(executionConfiguration).Init();
                    break;

                case BrowserType.Edge:
                    wrappedWebDriver = new EdgeBrowser(executionConfiguration).Init();
                    break;

                case BrowserType.EdgeHeadless:
                    wrappedWebDriver = new EdgeBrowserHeadless(executionConfiguration).Init();
                    break;

                case BrowserType.InternetExplorer:
                    wrappedWebDriver = new InternetExplorerBrowser(executionConfiguration).Init();
                    break;

                case BrowserType.Safari:
                    wrappedWebDriver = new SafariBrowser(executionConfiguration).Init();
                    break;

                default:
                    throw new NotSupportedException($"Not supported browser {executionConfiguration.BrowserType}");
            }

            return wrappedWebDriver;
        }

        private static void ChangeWindowSize(Size windowSize, IWebDriver wrappedWebDriver)
        {
            if (windowSize != default)
            {
                wrappedWebDriver.Manage().Window.Size = windowSize;
            }
            else
            {
                wrappedWebDriver.Manage().Window.Maximize();
            }
        }

        private static int GetFreeTcpPort()
        {
            Thread.Sleep(100);
            var tcpListener = new TcpListener(IPAddress.Loopback, 0);
            tcpListener.Start();
            int port = ((IPEndPoint)tcpListener.LocalEndpoint).Port;
            tcpListener.Stop();
            return port;
        }

        // TODO
        private static string[] GetExperimentalOptions()
        {
            var downloadDirectory = Path.Combine("home", Environment.UserName, "Downloads");
            var values = new string[]
            {
            "profile.default_content_settings.popups@2",
            $"download.default_directory@/{downloadDirectory}",
            "download.prompt_for_download@2",
            "download.directory_upgrade@1",
            "safebrowsing.enabled@2",
            "plugins.always_open_pdf_externally@1",
            "plugins.plugins_disabled@newList<string>{'Chrome PDF Viewer'}",
            };

            return values;
        }
    }
}

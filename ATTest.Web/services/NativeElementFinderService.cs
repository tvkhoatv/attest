﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace ATTest.Web
{
    public class NativeElementFinderService : IWebDriverElementFinderService
    {
        private readonly ISearchContext _searchContext;

        public NativeElementFinderService(ISearchContext searchContext) => _searchContext = searchContext;

        public IWebElement Find<TBy>(TBy by)
            where TBy : FindStrategy
        {
            var element = _searchContext.FindElement(by.Convert());

            return element;
        }

        public IEnumerable<IWebElement> FindAll<TBy>(TBy by)
            where TBy : FindStrategy
        {
            IEnumerable<IWebElement> result = _searchContext.FindElements(@by.Convert());

            return result;
        }
    }
}

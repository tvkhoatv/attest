﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using OpenQA.Selenium;

namespace ATTest.Web;

public class JavaScriptService : WebService
{
    public JavaScriptService(IWebDriver wrappedDriver) : base(wrappedDriver) {}

    public object Execute<TComponent>(string script, TComponent element, params object[] args) where TComponent : Component
    {
        try
        {
            var javaScriptExecutor = WrappedDriver as IJavaScriptExecutor;
            var result = javaScriptExecutor?.ExecuteScript(script, element.WrappedElement, args);

            return result;
        }
        catch (NullReferenceException)
        {
            throw;
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            return string.Empty;
        }
    }

    public object Execute(string script)
    {
        try
        {
            var javaScriptExecutor = WrappedDriver as IJavaScriptExecutor;
            var result = javaScriptExecutor?.ExecuteScript(script);

            return result?.ToString();
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            return string.Empty;
        }
    }

    public string Execute(string script, IWebElement nativeElement)
    {
        try
        {
            var javaScriptExecutor = WrappedDriver as IJavaScriptExecutor;
            var result = javaScriptExecutor?.ExecuteScript(script, nativeElement);

            return result?.ToString();
        }
        catch (NullReferenceException)
        {
            throw;
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            return string.Empty;
        }
    }

    public void ExecuteAsync(string script, IWebElement nativeElement)
    {
        try
        {
            var javaScriptExecutor = WrappedDriver as IJavaScriptExecutor;
            javaScriptExecutor?.ExecuteAsyncScript(script, nativeElement);
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
        }
    }
}


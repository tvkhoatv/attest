﻿using OpenQA.Selenium;
using OpenQA.Selenium.Internal;

namespace ATTest.Web
{
    public class CookiesService : WebService
    {
        public CookiesService(IWebDriver wrappedDriver) : base(wrappedDriver)
        {
        }

        public void AddCookie(string cookieName, string cookieValue, string path = "/")
        {
            cookieValue = Uri.UnescapeDataString(cookieValue);
            var cookie = new Cookie(cookieName, cookieValue, path);
            WrappedDriver.Manage().Cookies.AddCookie(cookie);
        }

        public void AddCookie(System.Net.Cookie cookieToAdd)
        {
            var cookieValue = Uri.UnescapeDataString(cookieToAdd.Value);

            Cookie updatedCookie = new ReturnedCookie(
                cookieToAdd.Name,
                cookieValue,
                cookieToAdd.Domain,
                cookieToAdd.Path,
                cookieToAdd.Expires == default ? null : (DateTime?)cookieToAdd.Expires,
                cookieToAdd.Secure,
                cookieToAdd.HttpOnly);

            WrappedDriver.Manage().Cookies.AddCookie(updatedCookie);
        }

        public void DeleteAllCookies() => WrappedDriver.Manage().Cookies.DeleteAllCookies();

        public void DeleteCookie(string cookieName) => WrappedDriver.Manage().Cookies.DeleteCookieNamed(cookieName);

        public List<Cookie> GetAllCookies()
        {
            var cookies = new List<Cookie>();
            foreach (var currentCookie in WrappedDriver.Manage().Cookies.AllCookies)
            {
                cookies.Add(currentCookie);
            }

            return cookies;
        }

        public string GetCookie(string cookieName)
        {
            var cookie = WrappedDriver.Manage().Cookies.GetCookieNamed(cookieName);
            if (cookie != null)
            {
                return cookie.Value;
            }

            return string.Empty;
        }
    }
}

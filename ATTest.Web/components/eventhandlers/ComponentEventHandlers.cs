﻿using ATTest.Web.Contracts;
using ATTest.Web.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATTest.Web.Controls.EventHandlers
{
    public class ComponentEventHandlers : IControlEventHandlers
    {
        //protected DynamicTestCasesService DynamicTestCasesService => ServicesCollection.Current.Resolve<DynamicTestCasesService>();
        //protected BugReportingContextService BugReportingContextService => ServicesCollection.Current.Resolve<BugReportingContextService>();

        public virtual void SubscribeToAll()
        {
            Component.ScrollingToVisible += ScrollingToVisibleEventHandler;
            Component.ScrolledToVisible += ScrolledToVisibleEventHandler;
            Component.CreatingComponent += CreatingElementEventHandler;
            Component.CreatedComponent += CreatedElementEventHandler;
            Component.CreatingComponents += CreatingElementsEventHandler;
            Component.CreatedComponents += CreatedElementsEventHandler;
            Component.ReturningWrappedElement += ReturningWrappedElementEventHandler;
            Component.Focusing += FocusingEventHandler;
            Component.Focused += FocusedEventHandler;
        }

        public virtual void UnsubscribeToAll()
        {
            Component.ScrollingToVisible -= ScrollingToVisibleEventHandler;
            Component.ScrolledToVisible -= ScrolledToVisibleEventHandler;
            Component.CreatingComponent -= CreatingElementEventHandler;
            Component.CreatedComponent -= CreatedElementEventHandler;
            Component.CreatingComponents -= CreatingElementsEventHandler;
            Component.CreatedComponents -= CreatedElementsEventHandler;
            Component.ReturningWrappedElement -= ReturningWrappedElementEventHandler;
            Component.Focusing -= FocusingEventHandler;
            Component.Focused -= FocusedEventHandler;
        }

        protected virtual void ScrollingToVisibleEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void ScrolledToVisibleEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void CreatingElementEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void CreatedElementEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void CreatingElementsEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void CreatedElementsEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void ReturningWrappedElementEventHandler(object sender, NativeElementActionEventArgs arg)
        {
        }

        protected virtual void FocusingEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void FocusedEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void HoveringEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void HoveredEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void ClickingEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }

        protected virtual void ClickedEventHandler(object sender, ComponentActionEventArgs arg)
        {
        }
    }
}

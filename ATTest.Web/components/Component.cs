﻿using ATTest.Web.Contracts;
using ATTest.Web.Events;
using ATTest.Web.Untils;
using ATTest.Web.Waits;
using OpenQA.Selenium;
using System.Diagnostics;
using System.Drawing;
using System.Text;

namespace ATTest.Web;

public partial class Component : IComponent
{
    public static event EventHandler<ComponentActionEventArgs> Focusing;
    public static event EventHandler<ComponentActionEventArgs> Focused;

    private readonly ComponentWaitService _elementWaiter;
    private readonly List<WaitStrategy> _untils;
    private IWebElement _wrappedElement;

    public string TagName => WrappedElement.TagName;

    public Component()
    {
        _elementWaiter = new ComponentWaitService();
        WrappedDriver = ServicesCollection.Current.Resolve<IWebDriver>();

        _untils = new List<WaitStrategy>();
        JavaScriptService = ServicesCollection.Current.Resolve<JavaScriptService>();
        BrowserService = ServicesCollection.Current.Resolve<BrowserService>();
        ComponentService = ServicesCollection.Current.Resolve<ComponentCreateService>();
    }

    public static event EventHandler<ComponentActionEventArgs> ScrollingToVisible;
    public static event EventHandler<ComponentActionEventArgs> ScrolledToVisible;
    public static event EventHandler<ComponentActionEventArgs> SettingAttribute;
    public static event EventHandler<ComponentActionEventArgs> AttributeSet;

    public static event EventHandler<ComponentActionEventArgs> CreatingComponent;
    public static event EventHandler<ComponentActionEventArgs> CreatedComponent;
    public static event EventHandler<ComponentActionEventArgs> CreatingComponents;
    public static event EventHandler<ComponentActionEventArgs> CreatedComponents;
    public static event EventHandler<NativeElementActionEventArgs> ReturningWrappedElement;

    public IWebDriver WrappedDriver { get; }

    public IWebElement WrappedElement
    {
        get
        {
            var element = GetAndWaitWebDriverElement(ShouldCacheElement);
            ReturningWrappedElement?.Invoke(this, new NativeElementActionEventArgs(element));
            if (element == null)
            {
                throw new NullReferenceException($"The element with locator {By.Value} was not found. Probably you should change the locator or wait for a specific condition.");
            }

            return element;
        }
        set => _wrappedElement = value;
    }

    public IWebElement ParentWrappedElement { get; set; }
    public int ElementIndex { get; set; }
    internal bool ShouldCacheElement { get; set; }

    protected readonly JavaScriptService JavaScriptService;
    protected readonly BrowserService BrowserService;
    protected readonly ComponentCreateService ComponentService;

    public dynamic By { get; internal set; }

    public string GetTitle() => string.IsNullOrEmpty(GetAttribute("title")) ? null : GetAttribute("title");

    public string GetTabIndex() => string.IsNullOrEmpty(GetAttribute("tabindex")) ? null : GetAttribute("tabindex");

    public string GetAccessKey() => string.IsNullOrEmpty(GetAttribute("accesskey")) ? null : GetAttribute("accesskey");

    public string GetStyle() => string.IsNullOrEmpty(GetAttribute("style")) ? null : GetAttribute("style");

    public string GetDir() => string.IsNullOrEmpty(GetAttribute("dir")) ? null : GetAttribute("dir");

    public string GetLang() => string.IsNullOrEmpty(GetAttribute("lang")) ? null : GetAttribute("lang");

    //public AssertedFormPage AIAnalyze()
    //{
    //    string currentComponentScreenshot = TakeScreenshot();
    //    var formRecognizer = ServicesCollection.Current.Resolve<FormRecognizer>();
    //    var analyzedComponent = formRecognizer.Analyze(currentComponentScreenshot);
    //    return analyzedComponent;
    //}

    // TO DO.
    public string TakeScreenshot(string filePath = null)
    {
        return filePath;
    }

    public dynamic Create<TBy>(TBy by, Type newElementType) where TBy : FindStrategy
    {
        CreatingComponent?.Invoke(this, new ComponentActionEventArgs(this));

        var elementRepository = new ComponentRepository();
        var element = elementRepository.CreateComponentWithParent(by, WrappedElement, newElementType, ShouldCacheElement);

        CreatedComponent?.Invoke(this, new ComponentActionEventArgs(this));

        return element;
    }

    public TComponent Create<TComponent, TBy>(TBy by, bool shouldCacheElement = false)
        where TBy : FindStrategy
        where TComponent : Component
    {
        CreatingComponent?.Invoke(this, new ComponentActionEventArgs(this));

        var elementRepository = new ComponentRepository();
        var element = elementRepository.CreateComponentWithParent<TComponent>(by, WrappedElement, null, 0, shouldCacheElement);

        CreatedComponent?.Invoke(this, new ComponentActionEventArgs(this));

        return element;
    }

    public ComponentsList<TComponent> CreateAll<TComponent, TBy>(TBy by)
        where TBy : FindStrategy
        where TComponent : Component
    {
        CreatingComponents?.Invoke(this, new ComponentActionEventArgs(this));

        var elementsCollection = new ComponentsList<TComponent>(by, WrappedElement, ShouldCacheElement);

        CreatedComponents?.Invoke(this, new ComponentActionEventArgs(this));

        return elementsCollection;
    }

    public void WaitToBe() => GetAndWaitWebDriverElement(true);

    public bool IsPresent
    {
        get
        {
            try
            {
                return GetAndWaitWebDriverElement(false) != null;
            }
            catch
            {
                return false;
            }
        }
    }

    public bool IsVisible
    {
        get
        {
            if (IsPresent)
            {
                return WrappedElement.Displayed;
            }

            return false;
        }
    }

    public string CssClass
    {
        get
        {
            return GetClassAttribute();
        }
    }

    public string GetAttribute(string name)
    {
        return WrappedElement.GetAttribute(name);
    }

    public void ScrollToVisible()
    {
        ScrollToVisible(true);
    }

    public void SetAttribute(string name, string value)
    {
        SettingAttribute?.Invoke(this, new ComponentActionEventArgs(this));

        JavaScriptService.Execute(
            $"arguments[0].setAttribute('{name}', '{value}');", WrappedElement
        );

        AttributeSet?.Invoke(this, new ComponentActionEventArgs(this));
    }

    public void Focus()
    {
        Focusing?.Invoke(this, new ComponentActionEventArgs(this));

        JavaScriptService.Execute("window.focus();");
        JavaScriptService.Execute("arguments[0].focus();", WrappedElement);

        Focused?.Invoke(this, new ComponentActionEventArgs(this));
    }

    public string ComponentName { get; internal set; }

    public string PageName { get; internal set; }

    public virtual Type ComponentType => GetType();

    public Type LocatorType => By.GetType();

    public string LocatorValue => By.Value;

    public void Hide()
    {
        SetAttribute("style", "display:none");
    }

    public void EnsureState(WaitStrategy until)
    {
        _untils.Add(until);
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.AppendLine($"{ComponentName}");
        sb.AppendLine($"X = {Location.X}");
        sb.AppendLine($"Y = {Location.Y}");
        sb.AppendLine($"Height = {Size.Height}");
        sb.AppendLine($"Width = {Size.Width}");
        return sb.ToString();
    }

    private IWebElement GetAndWaitWebDriverElement(bool shouldCacheElement = false)
    {
        if (_wrappedElement != null && shouldCacheElement)
        {
            return _wrappedElement;
        }

        if (_untils.Count == 0 || _untils[0] == null)
        {
            EnsureState(Wait.To.Exists());
        }

        try
        {
            foreach (var until in _untils)
            {
                if (until != null)
                {
                    _elementWaiter.Wait(this, until);
                }

                if (until != null && until.GetType() == typeof(WaitNotToExistStrategy))
                {
                    return _wrappedElement;
                }
            }

            _wrappedElement = GetWebDriverElement(shouldCacheElement);

            ScrollToMakeElementVisible();
            if (ConfigurationService.GetSection<WebSettings>().ShouldWaitUntilReadyOnElementFound)
            {
                BrowserService.WaitUntilReady();
            }

            _untils.Clear();
        }
        catch (WebDriverTimeoutException)
        {
            throw new TimeoutException($"\n\nThe element: \n Name: '{ComponentName}', \n Locator: '{LocatorType.Name} = {LocatorValue}', \n Type: '{ComponentType.Name}' \nWas not found on the page or didn't fulfill the specified conditions.\n\n");
        }

        return _wrappedElement;
    }

    private void ScrollToMakeElementVisible()
    {
        // By default scroll down to make the element visible.
        if (WrappedWebDriverCreateService.BrowserConfiguration.ShouldAutomaticallyScrollToVisible)
        {
            ScrollToVisible(false);
        }
    }

    private IWebElement GetWebDriverElement(bool shouldCacheElement = false)
    {
        if (_wrappedElement != null && shouldCacheElement)
        {
            return _wrappedElement;
        }

        if (ParentWrappedElement == null && _wrappedElement == null)
        {
            var nativeElementFinderService = new NativeElementFinderService(WrappedDriver);
            return nativeElementFinderService.FindAll(By)[ElementIndex];
        }

        if (ParentWrappedElement != null)
        {
            var nativeElementFinderService = new NativeElementFinderService(ParentWrappedElement);
            return nativeElementFinderService.FindAll(By)[ElementIndex];
        }

        return _wrappedElement;
    }

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    public Point Location => WrappedElement.Location;

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    public Size Size => WrappedElement.Size;

    public string GetCssValue(string propertyName) => WrappedElement.GetCssValue(propertyName);

    protected virtual void DefaultSetAttribute(string attributeName, string attributeValue)
    {
        SettingAttribute?.Invoke(this, new ComponentActionEventArgs(this));

        JavaScriptService.Execute(
            $"arguments[0].setAttribute('{attributeName}', '{attributeValue}');", this);

        AttributeSet?.Invoke(this, new ComponentActionEventArgs(this));
    }

    protected virtual string GetClassAttribute()
    {
        return string.IsNullOrEmpty(WrappedElement.GetAttribute("class")) ? null : WrappedElement.GetAttribute("class");
    }

    internal void Click(EventHandler<ComponentActionEventArgs> clicking, EventHandler<ComponentActionEventArgs> clicked)
    {
        clicking?.Invoke(this, new ComponentActionEventArgs(this));

        PerformJsClick();

        clicked?.Invoke(this, new ComponentActionEventArgs(this));
    }

    private void PerformJsClick() => JavaScriptService.Execute("arguments[0].focus();arguments[0].click();", this);

    internal void Hover(EventHandler<ComponentActionEventArgs> hovering, EventHandler<ComponentActionEventArgs> hovered)
    {
        hovering?.Invoke(this, new ComponentActionEventArgs(this));

        JavaScriptService.Execute("arguments[0].onmouseover();", this);

        hovered?.Invoke(this, new ComponentActionEventArgs(this));
    }

    internal string GetInnerText()
    {
        return WrappedElement.Text.Trim().Replace("\r\n", string.Empty);
    }

    internal void SetValue(EventHandler<ComponentActionEventArgs> gettingValue, EventHandler<ComponentActionEventArgs> gotValue, string value)
    {
        gettingValue?.Invoke(this, new ComponentActionEventArgs(this, value));
        SetAttribute("value", value);
        gotValue?.Invoke(this, new ComponentActionEventArgs(this, value));
    }

    internal string DefaultGetValue()
    {
        return WrappedElement.GetAttribute("value");
    }

    internal int? DefaultGetMaxLength()
    {
        int? result = string.IsNullOrEmpty(GetAttribute("maxlength")) ? null : (int?)int.Parse(GetAttribute("maxlength"));
        if (result != null && (result == 2147483647 || result == -1))
        {
            result = null;
        }

        return result;
    }

    internal int? DefaultGetMinLength()
    {
        int? result = string.IsNullOrEmpty(GetAttribute("minlength")) ? null : (int?)int.Parse(GetAttribute("minlength"));

        if (result != null && result == -1)
        {
            result = null;
        }

        return result;
    }

    internal int? GetSizeAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("size")) ? null : (int?)int.Parse(GetAttribute("size"));
    }

    internal int? GetHeightAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("height")) ? null : (int?)int.Parse(GetAttribute("height"));
    }

    internal int? GetWidthAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("width")) ? null : (int?)int.Parse(GetAttribute("width"));
    }

    internal string GetInnerHtmlAttribute()
    {
        return WrappedElement.GetAttribute("innerHTML");
    }

    internal string GetForAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("for")) ? null : GetAttribute("for");
    }

    protected bool GetDisabledAttribute()
    {
        string valueAttr = WrappedElement.GetAttribute("disabled");
        return valueAttr == "true";
    }

    internal string GetText()
    {
        return WrappedElement.Text;
    }

    internal int? GetMinAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("min")) ? null : (int?)int.Parse(GetAttribute("min"));
    }

    internal int? GetMaxAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("max")) ? null : (int?)int.Parse(GetAttribute("max"));
    }

    internal string GetMinAttributeAsString()
    {
        return string.IsNullOrEmpty(GetAttribute("min")) ? null : GetAttribute("min");
    }

    internal string GetMaxAttributeAsString()
    {
        return string.IsNullOrEmpty(GetAttribute("max")) ? null : GetAttribute("max");
    }

    internal int? GetStepAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("step")) ? null : (int?)int.Parse(GetAttribute("step"));
    }

    internal string GetPlaceholderAttribute()
    {
        return string.IsNullOrEmpty(GetAttribute("placeholder")) ? null : GetAttribute("placeholder");
    }

    internal bool GetAutoCompleteAttribute()
    {
        return GetAttribute("autocomplete") == "on";
    }

    internal bool GetReadonlyAttribute()
    {
        return !string.IsNullOrEmpty(GetAttribute("readonly"));
    }

    internal bool GetRequiredAttribute()
    {
        return !string.IsNullOrEmpty(GetAttribute("required"));
    }

    internal string GetList()
    {
        return string.IsNullOrEmpty(GetAttribute("list")) ? null : GetAttribute("list");
    }

    internal void DefaultSetText(EventHandler<ComponentActionEventArgs> settingValue, EventHandler<ComponentActionEventArgs> valueSet, string value)
    {
        settingValue?.Invoke(this, new ComponentActionEventArgs(this, value));

        Thread.Sleep(50);

        // (Anton: 11.09.2019): We do this for optimization purposes so that we don't make two WebDriver calls.
        var wrappedElement = WrappedElement;
        try
        {
            wrappedElement.Clear();
        }
        catch
        {
            // ignore
        }

        Thread.Sleep(50);
        try
        {
            wrappedElement.SendKeys(value);
        }
        catch (StaleElementReferenceException)
        {
            WrappedElement.SendKeys(value);
        }

        valueSet?.Invoke(this, new ComponentActionEventArgs(this, value));
    }

    private void ScrollToVisible(bool shouldWait = true)
    {
        ScrollingToVisible?.Invoke(this, new ComponentActionEventArgs(this));
        try
        {
            var wrappedElement = _wrappedElement ?? WrappedElement;
            JavaScriptService.Execute("arguments[0].scrollIntoView({block:'center'});", wrappedElement);
            if (shouldWait)
            {
                Thread.Sleep(500);
                //this.ToExists().WaitToBe();
            }
        }
        catch (ElementNotInteractableException)
        {
            // ignore
        }

        ScrolledToVisible?.Invoke(this, new ComponentActionEventArgs(this));
    }
}

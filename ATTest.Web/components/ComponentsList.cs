﻿using System;
using System.Collections;
using System.Collections.Generic;
using ATTest.Web.Waits;
using ATTest.Utilities;
using OpenQA.Selenium;
using System.Diagnostics;

namespace ATTest.Web
{
    public class ComponentsList<TComponent> : IEnumerable<TComponent> where TComponent : Component
    {
        private readonly FindStrategy _by;
        private readonly IWebElement _parenTComponent;
        private readonly List<TComponent> _foundElements;
        private readonly bool _shouldCacheFoundElements;
        private List<Component> _cachedElements;

        public ComponentsList(
            FindStrategy by,
            IWebElement parenTComponent,
            bool shouldCacheFoundElements) : this(by, parenTComponent)
        {
            _shouldCacheFoundElements = shouldCacheFoundElements;
        }

        public ComponentsList()
        {
            _foundElements = new List<TComponent>();
            WrappedDriver = ServicesCollection.Current.Resolve<IWebDriver>();
        }

        public ComponentsList(IEnumerable<TComponent> nativeElementList)
        {
            _foundElements = new List<TComponent>(nativeElementList);
            WrappedDriver = ServicesCollection.Current.Resolve<IWebDriver>();
        }

        public ComponentsList(FindStrategy by, IWebElement parenTComponent)
        {
            By = by;
            ParenTComponent = parenTComponent;
        }

        public IWebDriver WrappedDriver { get; }
        public FindStrategy By { get; }
        public IWebElement ParenTComponent { get; }

        public TComponent this[int i]
        {
            get
            {
                try
                {
                    return GetAndWaitWebDriverElements().ElementAt(i);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    throw new Exception($"Component at position {i} was not found.", ex);
                }
            }
        }

        //public IEnumerator<Component> GetEnumerator() => GetAndWaitWebDriverElements().GetEnumerator();

        //IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<TComponent> GetEnumerator()
        {
            return GetAndWaitWebDriverElements().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() // Explicitly implement the non-generic version.
        {
            return GetAndWaitWebDriverElements().GetEnumerator();
        }

        public int Count()
        {
            if (_by == null)
            {
                return _foundElements.Count;
            }

            var nativeElements = WaitWebDriverElements();
            return nativeElements.Count();
        }

        public void ForEach(Action<TComponent> action)
        {
            foreach (var tableRow in this)
            {
                action(tableRow);
            }
        }

        public void Add(TComponent element)
        {
            _foundElements.Add(element);
        }

        private IEnumerable<TComponent> GetAndWaitWebDriverElements()
        {
            if (_shouldCacheFoundElements && _cachedElements == null)
            {
                try
                {
                    _cachedElements = (List<Component>?)GetAndWaitNativeElements();
                }
                catch(Exception ex)
                {

                }
            }

            if (_shouldCacheFoundElements && _cachedElements != null)
            {
                foreach (var element in _cachedElements)
                {
                    yield return (TComponent)element;
                }
            }
            else
            {
                foreach (var element in GetAndWaitNativeElements())
                {
                    yield return element;
                }
            }

            IEnumerable<TComponent> GetAndWaitNativeElements()
            {
                foreach (var foundElement in _foundElements)
                {
                    yield return foundElement;
                }

                if (_by != null)
                {
                    var nativeElements = WaitWebDriverElements();
                    int index = 0;

                    foreach (var nativeElement in nativeElements)
                    {
                        var elementRepository = new ComponentRepository();
                        if (_parenTComponent != null)
                        {
                            var element = elementRepository.CreateComponentWithParent<TComponent>(_by, _parenTComponent, nativeElement, index++, _shouldCacheFoundElements);
                            yield return element;
                        }
                        else
                        {
                            var element = elementRepository.CreateComponentThatIsFound<TComponent>(_by, nativeElement, _shouldCacheFoundElements);
                            yield return element;
                        }
                    }
                }
            }
        }

        private IEnumerable<IWebElement> WaitWebDriverElements()
        {
            var elementFinder = _parenTComponent == null
                ? new NativeElementFinderService(WrappedDriver)
                : new NativeElementFinderService(_parenTComponent);
            var elementWaiter = new ComponentWaitService();
            if (_parenTComponent == null)
            {
                return ConditionalWait(elementFinder);
            }
            else
            {
                var elementRepository = new ComponentRepository();
                var parenTComponent = elementRepository.CreateComponentThatIsFound<Component>(_by, _parenTComponent, true);

                return ConditionalWait(elementFinder);
            }
        }

        // TODO: This is a technical debt, for the case with _by that is not null and we want to verify the non-existance of elements
        public IEnumerable<IWebElement> ConditionalWait(NativeElementFinderService elementFinder)
        {
            ATTest.Utilities.Wait.ForConditionUntilTimeout(
                () =>
                {
                    return elementFinder.FindAll(_by).Any();
                },
                totalRunTimeoutMilliseconds: ConfigurationService.GetSection<WebSettings>().TimeoutSettings.ElementToExistTimeout,
                sleepTimeMilliseconds: ConfigurationService.GetSection<WebSettings>().TimeoutSettings.SleepInterval
            );

            return elementFinder.FindAll(_by);
        }

        public void AddRange(List<TComponent> currentFilteredCells)
        {
            _foundElements.AddRange(currentFilteredCells);
        }
    }
}

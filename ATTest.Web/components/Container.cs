﻿using System.Diagnostics;

namespace ATTest.Web.Controls
{
    public class Container : Component
    {
        public new Action Hover { get; set; }

        public new Action Focus { get; set; }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Func<string> InnerText { get; set; }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public Func<string> InnerHtml { get; set; }
    }
}

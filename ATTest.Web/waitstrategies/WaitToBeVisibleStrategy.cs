﻿using System;
using OpenQA.Selenium;

namespace ATTest.Web.Untils
{
    public class WaitToBeVisibleStrategy : WaitStrategy
    {
        public WaitToBeVisibleStrategy(int? timeoutInterval = null, int? sleepInterval = null) : base(timeoutInterval, sleepInterval)
        {
            TimeoutInterval = timeoutInterval ?? ConfigurationService.GetSection<WebSettings>().TimeoutSettings.ElementToBeVisibleTimeout;
        }

        public override void WaitUntil<TBy>(TBy by)
        {
            WaitUntil(d => ElementIsVisible(WrappedWebDriver, by), TimeoutInterval, SleepInterval);
        }

        public override void WaitUntil<TBy>(TBy by, Component parent)
        {
            WaitUntil(d => ElementIsVisible(parent.WrappedElement, by), TimeoutInterval, SleepInterval);
        }

        private bool ElementIsVisible<TBy>(ISearchContext searchContext, TBy by) where TBy : FindStrategy
        {
            try
            {
                var element = searchContext.FindElement(by.Convert());
                return element != null && element.Displayed;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}

﻿using OpenQA.Selenium;
using System;

namespace ATTest.Web.Untils
{
    public class WaitToBeClickableStrategy : WaitStrategy
    {
        public WaitToBeClickableStrategy(int? timeoutInterval = null, int? sleepInterval = null) : base(timeoutInterval, sleepInterval)
        {
            TimeoutInterval = timeoutInterval ?? ConfigurationService.GetSection<WebSettings>().TimeoutSettings.ElementToBeClickableTimeout;
        }

        public override void WaitUntil<TBy>(TBy by)
        {
            WaitUntil(x => ElementIsClickable(WrappedWebDriver, by), TimeoutInterval, SleepInterval);
        }

        public override void WaitUntil<TBy>(TBy by, Component parent)
        {
            WaitUntil(x => ElementIsClickable(parent.WrappedElement, by), TimeoutInterval, SleepInterval);
        }

        private bool ElementIsClickable<TBy>(ISearchContext searchContext, TBy by) where TBy : FindStrategy
        {
            var element = searchContext.FindElement(by.Convert());
            try
            {
                return element != null && element.Enabled;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
        }
    }
}

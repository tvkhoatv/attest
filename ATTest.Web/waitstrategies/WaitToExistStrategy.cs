﻿using OpenQA.Selenium;

namespace ATTest.Web.Untils
{
    public class WaitToExistStrategy : WaitStrategy
    {
        public WaitToExistStrategy(int? timeoutInterval = null, int? sleepInterval = null) : base(timeoutInterval, sleepInterval)
        {
            TimeoutInterval = timeoutInterval ?? ConfigurationService.GetSection<WebSettings>().TimeoutSettings.ElementToExistTimeout;
        }

        public override void WaitUntil<TBy>(TBy by)
        {
            WaitUntil(d => ElementExists(WrappedWebDriver, by), TimeoutInterval, SleepInterval);
        }

        public override void WaitUntil<TBy>(TBy by, Component parent)
        {
            WaitUntil(d => ElementExists(parent.WrappedElement, by), TimeoutInterval, SleepInterval);
        }

        private bool ElementExists<TBy>(ISearchContext searchContext, TBy by) where TBy : FindStrategy
        {
            try
            {
                var element = searchContext.FindElement(by.Convert());
                return element != null;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATTest.Web.Untils;

public abstract class WaitStrategy
{
    protected WaitStrategy(int? timeoutInterval = null, int? sleepInterval = null)
    {
        WrappedWebDriver = ServicesCollection.Current.Resolve<IWebDriver>();
        TimeoutInterval = timeoutInterval;
        SleepInterval = sleepInterval ?? ConfigurationService.GetSection<WebSettings>().TimeoutSettings.SleepInterval;
    }

    protected IWebDriver WrappedWebDriver { get; }

    protected int? TimeoutInterval { get; set; }

    protected int? SleepInterval { get; }

    public abstract void WaitUntil<TBy>(TBy by) where TBy : FindStrategy;

    public abstract void WaitUntil<TBy>(TBy by, Component parent) where TBy : FindStrategy;

    protected void WaitUntil(Func<IWebDriver, bool> waitCondition, int? timeout, int? sleepInterval)
    {
        if (timeout != null && sleepInterval != null)
        {
            var timeoutTimeSpan = TimeSpan.FromSeconds((int)timeout);
            var sleepIntervalTimeSpan = TimeSpan.FromSeconds((int)sleepInterval);
            var wait = new WebDriverWait(new SystemClock(), WrappedWebDriver, timeoutTimeSpan, sleepIntervalTimeSpan);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            wait.Until(waitCondition);
        }
    }

    protected void WaitUntil(Func<IWebDriver, IWebElement> waitCondition, int? timeout, int? sleepInterval)
    {
        if (timeout != null && sleepInterval != null)
        {
            var timeoutTimeSpan = TimeSpan.FromSeconds((int)timeout);
            var sleepIntervalTimeSpan = TimeSpan.FromSeconds((int)sleepInterval);
            var wait = new WebDriverWait(new SystemClock(), WrappedWebDriver, timeoutTimeSpan, sleepIntervalTimeSpan);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            wait.Until(waitCondition);
        }
    }

    protected IWebElement FindElement<TBy>(ISearchContext searchContext, TBy by) where TBy : FindStrategy
    {
        var nativeElementFinder = new NativeElementFinderService(searchContext);
        var element = nativeElementFinder.Find(by);
        return element;
    }
}

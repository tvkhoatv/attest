﻿namespace ATTest.Web
{
    internal class Wait
    {
        static Wait() => To = new WaitStrategyFactory();

        public static WaitStrategyFactory To { get; }
    }
}

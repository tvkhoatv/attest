﻿using OpenQA.Selenium;
using System;

namespace ATTest.Web.Untils
{
    public class WaitToHaveContentStrategy : WaitStrategy
    {
        public WaitToHaveContentStrategy(int? timeoutInterval = null, int? sleepInterval = null) : base(timeoutInterval, sleepInterval)
        {
            TimeoutInterval = timeoutInterval ?? ConfigurationService.GetSection<WebSettings>().TimeoutSettings.ElementToHaveContentTimeout;
        }

        public override void WaitUntil<TBy>(TBy by)
        {
            WaitUntil(d => ElementHasContent(WrappedWebDriver, by), TimeoutInterval, SleepInterval);
        }

        public override void WaitUntil<TBy>(TBy by, Component parent)
        {
            WaitUntil(d => ElementHasContent(parent.WrappedElement, by), TimeoutInterval, SleepInterval);
        }

        private bool ElementHasContent<TBy>(ISearchContext searchContext, TBy by) where TBy : FindStrategy
        {
            try
            {
                var element = searchContext.FindElement(by.Convert());
                return element != null && !string.IsNullOrEmpty(element.Text);
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
        }
    }
}

﻿using System.Web;
using SeleniumExtras.PageObjects;

namespace ATTest.Web
{
    public abstract class WebPage
    {
        protected dynamic _urlSettings;

        public App App;

        #region Constructor
        public WebPage()
        {
            App = ServicesCollection.Current.Resolve<App>();
            PageFactory.InitElements(App.Browser.GetDriver(), this);
        }
        #endregion

        public virtual string Url { get; set; }

        public virtual void Open()
        {
            var baseUrl = Url;
            if (_urlSettings != null)
            {
                if (Url == null)
                    baseUrl = _urlSettings.BaseUrl;
                else
                {
                    if (!Url.Contains("http") || !Url.Contains("https"))
                    {
                        baseUrl = _urlSettings.BaseUrl + baseUrl;
                    }
                }
            }

            App.Navigation.Navigate(new Uri(baseUrl));
        }

        public void AssertLandedOnPage(string partialUrl, bool shouldUrlEncode = false)
        {
            if (shouldUrlEncode)
            {
                partialUrl = HttpUtility.UrlEncode(partialUrl);
            }

            App.Browser.WaitUntilReady();

            var currentBrowserUrl = App.Browser.Url.ToString();

            App.Assert.IsTrue(currentBrowserUrl.ToLower().Contains(partialUrl.ToLower()), $"The expected partialUrl: '{partialUrl}' was not found in the PageUrl: '{currentBrowserUrl}'");
        }

        public void AssertNotLandedOnPage(string partialUrl, bool shouldUrlEncode = false)
        {
            if (shouldUrlEncode)
            {
                partialUrl = HttpUtility.UrlEncode(partialUrl);
            }

            var currentBrowserUrl = App.Navigation.WrappedDriver.Url.ToString();
            App.Assert.IsFalse(currentBrowserUrl.Contains(partialUrl), $"The expected partialUrl: '{partialUrl}' was found in the PageUrl: '{currentBrowserUrl}'");
        }

        public void AssertUrl(string fullUrl)
        {
            var currentBrowserUrl = App.Browser.Url.ToString();
            Uri actualUri = new Uri(currentBrowserUrl);
            Uri expectedUri = new Uri(fullUrl);

            App.Assert.AreEqual(expectedUri.AbsoluteUri, actualUri.AbsoluteUri, $"Expected URL is different than the Actual one.");
        }

        public void AssertUrlPath(string urlPath)
        {
            var currentBrowserUrl = App.Navigation.WrappedDriver.Url.ToString();
            Uri actualUri = new Uri(currentBrowserUrl);

            App.Assert.AreEqual(urlPath, actualUri.AbsolutePath, $"Expected URL path is different than the Actual one.");
        }

        public virtual void AssertUrlPathAndQuery(string pathAndQuery)
        {
            var currentBrowserUrl = App.Navigation.WrappedDriver.Url.ToString();
            Uri actualUri = new Uri(currentBrowserUrl);

            App.Assert.AreEqual(pathAndQuery, actualUri.PathAndQuery, $"Expected URL is different than the Actual one.");
        }
    }
}

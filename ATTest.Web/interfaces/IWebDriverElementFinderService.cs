﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace ATTest.Web
{
    public interface IWebDriverElementFinderService
    {
        IWebElement Find<TBy>(TBy by) where TBy : FindStrategy;

        IEnumerable<IWebElement> FindAll<TBy>(TBy by) where TBy : FindStrategy;
    }
}

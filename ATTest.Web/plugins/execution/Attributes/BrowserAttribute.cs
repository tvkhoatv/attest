﻿using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using ATTest.Web.Enums;
using ATTest.Web.Services;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Safari;

namespace ATTest.Web;

[DebuggerDisplay("BrowserAttribute")]
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
public class BrowserAttribute : Attribute
{
    public BrowserAttribute(
        BrowserType browser,
        Lifecycle lifecycle = Lifecycle.NotSet,
        bool shouldAutomaticallyScrollToVisible = true,
        bool shouldCaptureHttpTraffic = false,
        bool shouldDisableJavaScript = false
    )
    {
        OS = OS.Windows;
        Browser = browser;
        Lifecycle = lifecycle;
        ShouldCaptureHttpTraffic = shouldCaptureHttpTraffic;
        ShouldDisableJavaScript = shouldDisableJavaScript;
        Size = default;
        ExecutionType = ExecutionType.Regular;
        ShouldAutomaticallyScrollToVisible = shouldAutomaticallyScrollToVisible && ConfigurationService.GetSection<WebSettings>().ShouldAutomaticallyScrollToVisible;
    }

    public BrowserAttribute(
        BrowserType browser,
        int width,
        int height,
        Lifecycle behavior = Lifecycle.NotSet,
        bool shouldAutomaticallyScrollToVisible = true,
        bool shouldCaptureHttpTraffic = false,
        bool shouldDisableJavaScript = false
    )
    {
        OS = OS.Windows;
        Browser = browser;
        Lifecycle = behavior;
        ShouldCaptureHttpTraffic = shouldCaptureHttpTraffic;
        ShouldDisableJavaScript = shouldDisableJavaScript;
        Size = new Size(width, height);
        ExecutionType = ExecutionType.Regular;
        ShouldAutomaticallyScrollToVisible = shouldAutomaticallyScrollToVisible && ConfigurationService.GetSection<WebSettings>().ShouldAutomaticallyScrollToVisible;
    }

    public BrowserAttribute(
        OS oS, BrowserType browser,
        Lifecycle behavior = Lifecycle.NotSet,
        bool shouldAutomaticallyScrollToVisible = true,
        bool shouldCaptureHttpTraffic = false,
        bool shouldDisableJavaScript = false
    )
    {
        OS = oS;
        Browser = browser;
        Lifecycle = behavior;
        ShouldCaptureHttpTraffic = shouldCaptureHttpTraffic;
        ShouldDisableJavaScript = shouldDisableJavaScript;
        Size = default;
        ExecutionType = ExecutionType.Regular;
        ShouldAutomaticallyScrollToVisible = shouldAutomaticallyScrollToVisible && ConfigurationService.GetSection<WebSettings>().ShouldAutomaticallyScrollToVisible;
    }

    public BrowserAttribute(
        OS oS, BrowserType browser,
        int width, int height,
        Lifecycle behavior = Lifecycle.NotSet,
        bool shouldAutomaticallyScrollToVisible = true,
        bool shouldCaptureHttpTraffic = false,
        bool shouldDisableJavaScript = false
    )
    {
        OS = oS;
        Browser = browser;
        Lifecycle = behavior;
        ShouldCaptureHttpTraffic = shouldCaptureHttpTraffic;
        ShouldDisableJavaScript = shouldDisableJavaScript;
        Size = new Size(width, height);
        ExecutionType = ExecutionType.Regular;
        ShouldAutomaticallyScrollToVisible = shouldAutomaticallyScrollToVisible && ConfigurationService.GetSection<WebSettings>().ShouldAutomaticallyScrollToVisible;
    }

    public BrowserAttribute(BrowserType browser, MobileWindowSize mobileWindowSize, Lifecycle behavior = Lifecycle.NotSet, bool shouldAutomaticallyScrollToVisible = true, bool shouldCaptureHttpTraffic = false, bool shouldDisableJavaScript = false)
        : this(browser, behavior, shouldCaptureHttpTraffic, shouldDisableJavaScript)
        => Size = WindowsSizeResolver.GetWindowSize(mobileWindowSize);

    public BrowserAttribute(BrowserType browser, TabletWindowSize tabletWindowSize, Lifecycle behavior = Lifecycle.NotSet, bool shouldAutomaticallyScrollToVisible = true, bool shouldCaptureHttpTraffic = false, bool shouldDisableJavaScript = false)
        : this(browser, behavior, shouldCaptureHttpTraffic, shouldDisableJavaScript)
        => Size = WindowsSizeResolver.GetWindowSize(tabletWindowSize);

    public BrowserAttribute(BrowserType browser, DesktopWindowSize desktopWindowSize, Lifecycle behavior = Lifecycle.NotSet, bool shouldAutomaticallyScrollToVisible = true, bool shouldCaptureHttpTraffic = false, bool shouldDisableJavaScript = false)
    : this(browser, behavior, shouldCaptureHttpTraffic, shouldDisableJavaScript)
        => Size = WindowsSizeResolver.GetWindowSize(desktopWindowSize);

    public BrowserAttribute(OS oS, BrowserType browser, MobileWindowSize mobileWindowSize, Lifecycle behavior = Lifecycle.NotSet, bool shouldAutomaticallyScrollToVisible = true, bool shouldCaptureHttpTraffic = false, bool shouldDisableJavaScript = false)
        : this(oS, browser, behavior, shouldCaptureHttpTraffic, shouldDisableJavaScript)
        => Size = WindowsSizeResolver.GetWindowSize(mobileWindowSize);

    public BrowserAttribute(OS oS, BrowserType browser, TabletWindowSize tabletWindowSize, Lifecycle behavior = Lifecycle.NotSet, bool shouldAutomaticallyScrollToVisible = true, bool shouldCaptureHttpTraffic = false, bool shouldDisableJavaScript = false)
        : this(oS, browser, behavior, shouldCaptureHttpTraffic, shouldDisableJavaScript)
        => Size = WindowsSizeResolver.GetWindowSize(tabletWindowSize);

    public BrowserAttribute(OS oS, BrowserType browser, DesktopWindowSize desktopWindowSize, Lifecycle behavior = Lifecycle.NotSet, bool shouldAutomaticallyScrollToVisible = true, bool shouldCaptureHttpTraffic = false, bool shouldDisableJavaScript = false)
        : this(oS, browser, behavior, shouldCaptureHttpTraffic, shouldDisableJavaScript)
        => Size = WindowsSizeResolver.GetWindowSize(desktopWindowSize);

    public BrowserType Browser { get; }

    public Lifecycle Lifecycle { get; }

    public Size Size { get; }

    public bool ShouldCaptureHttpTraffic { get; }
    public bool ShouldDisableJavaScript { get; }

    public ExecutionType ExecutionType { get; protected set; }

    public OS OS { get; internal set; }

    public bool ShouldAutomaticallyScrollToVisible { get; }
    public bool IsLighthouseEnabled { get; protected set; }

    protected string GetTestFullName(MemberInfo memberInfo, Type testClassType)
    {
        string testFullName = $"{testClassType.FullName}.{memberInfo.Name}";
        string testName = testFullName != null ? testFullName.Replace(" ", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(",", string.Empty).Replace("\"", string.Empty) : testClassType.FullName;
        return testName;
    }

    protected TDriverOptions AddAdditionalCapabilities<TDriverOptions>(Type type, TDriverOptions driverOptions)
        where TDriverOptions : DriverOptions, new()
    {
        var additionalCaps = ServicesCollection.Current.Resolve<Dictionary<string, object>>($"caps-{type.FullName}");
        if (additionalCaps != null)
        {
            foreach (var key in additionalCaps.Keys)
            {
                driverOptions.AddAdditionalOption(key, additionalCaps[key]);
            }
        }

        return driverOptions;
    }

    protected dynamic GetDriverOptionsBasedOnBrowser(Type type)
    {
        dynamic driverOptions;
        switch (Browser)
        {
            case BrowserType.Chrome:
            case BrowserType.ChromeHeadless:
                driverOptions = ServicesCollection.Current.Resolve<ChromeOptions>(type.FullName) ?? new ChromeOptions();
                break;
            case BrowserType.Firefox:
            case BrowserType.FirefoxHeadless:
                driverOptions = ServicesCollection.Current.Resolve<FirefoxOptions>(type.FullName) ?? new FirefoxOptions();
                var firefoxProfile = ServicesCollection.Current.Resolve<FirefoxProfile>(type.FullName);

                if (firefoxProfile != null)
                {
                    driverOptions.Profile = firefoxProfile;
                }

                break;
            case BrowserType.InternetExplorer:
                driverOptions = ServicesCollection.Current.Resolve<InternetExplorerOptions>(type.FullName) ?? new InternetExplorerOptions();
                break;
            case BrowserType.Edge:
                driverOptions = ServicesCollection.Current.Resolve<EdgeOptions>(type.FullName) ?? new EdgeOptions();
                break;
            case BrowserType.Safari:
                driverOptions = ServicesCollection.Current.Resolve<SafariOptions>(type.FullName) ?? new SafariOptions();
                break;
            default:
                {
                    throw new ArgumentException("You should specify a browser.");
                }
        }

        return driverOptions;
    }
}
﻿using System;
using System.Reflection;

namespace ATTest.Web.Plugins.Browser
{
    public interface IDriverOptionsAttribute
    {
        dynamic CreateOptions(MemberInfo memberInfo, Type testClassType);
    }
}
﻿using System;
using OpenQA.Selenium;

namespace ATTest.Web.Contracts;

public interface IComponent
{
    string ComponentName { get; }
    string PageName { get; }
    IWebElement WrappedElement { get; }
    Type ComponentType { get; }
    Type LocatorType { get; }
    string LocatorValue { get; }

    bool IsVisible { get; }
}

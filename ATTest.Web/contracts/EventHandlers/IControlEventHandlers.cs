﻿namespace ATTest.Web.Contracts
{
    public interface IControlEventHandlers
    {
        void SubscribeToAll();
        void UnsubscribeToAll();
    }
}

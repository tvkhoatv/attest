﻿using System;
//using ATTest.Web.Screenshots;

namespace ATTest.Web.MSTest
{
    public abstract class WebTest : MSTestBaseTest
    {
        private static readonly object _lockObject = new object();
        private static bool _arePluginsAlreadyInitialized;

        public App App { get; private set; }

        public override void Initialize()
        {
            App = ServicesCollection.Current.FindCollection(TestContext.FullyQualifiedTestClassName).Resolve<App>();
        }

        public override void Configure()
        {
            lock (_lockObject)
            {
                if (!_arePluginsAlreadyInitialized)
                {
                    //MSTestPluginConfiguration.Add();
                    //ExecutionTimePlugin.Add();
                    //VideoRecorderPluginConfiguration.AddMSTest();
                    //ScreenshotsPluginConfiguration.AddMSTest();

                    //DynamicTestCasesPlugin.Add();
                    //AllurePlugin.Add();
                    //BugReportingPlugin.Add();

                    WebPluginsConfiguration.AddBrowserLifecycle();
                    WebPluginsConfiguration.AddLogExecutionLifecycle();
                    WebPluginsConfiguration.AddControlDataHandlers();
                    //WebPluginsConfiguration.AddValidateExtensionsBddLogging();
                    //WebPluginsConfiguration.AddValidateExtensionsDynamicTestCases();
                    //WebPluginsConfiguration.AddValidateExtensionsBugReporting();
                    //WebPluginsConfiguration.AddLayoutAssertionExtensionsBddLogging();
                    //WebPluginsConfiguration.AddLayoutAssertionExtensionsDynamicTestCases();
                    //WebPluginsConfiguration.AddLayoutAssertionExtensionsBugReporting();
                    //WebPluginsConfiguration.AddElementsBddLogging();
                    //WebPluginsConfiguration.AddDynamicTestCases();
                    //WebPluginsConfiguration.AddBugReporting();
                    //WebPluginsConfiguration.AddHighlightComponents();
                    //WebPluginsConfiguration.AddMSTestGoogleLighthouse();
                    WebPluginsConfiguration.AddJavaScriptErrorsPlugin();

                    //APIPluginsConfiguration.AddAssertExtensionsBddLogging();
                    //APIPluginsConfiguration.AddApiAssertExtensionsDynamicTestCases();
                    //APIPluginsConfiguration.AddAssertExtensionsBugReporting();
                    //APIPluginsConfiguration.AddApiAuthenticationStrategies();
                    //APIPluginsConfiguration.AddRetryFailedRequests();
                    //APIPluginsConfiguration.AddLogExecution();

                    //if (ConfigurationService.GetSection<WebSettings>().FullPageScreenshotsEnabled)
                    //{
                    //    WebScreenshotPluginConfiguration.UseFullPageScreenshotsOnFail();
                    //}
                    //else
                    //{
                    //    WebScreenshotPluginConfiguration.UseVanillaWebDriverScreenshotsOnFail();
                    //}

                    _arePluginsAlreadyInitialized = true;
                }
            }
        }
    }
}
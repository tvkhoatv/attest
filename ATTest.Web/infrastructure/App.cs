﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using OpenQA.Selenium;
using ATTest.Assertions;
using ATTest.Web.Services;
using ATTest.Web.Controls.EventHandlers;
using ATTest.Plugins;

namespace ATTest.Web;

public class App : IDisposable
{
    private bool disposedValue;

    public BrowserService Browser => ServicesCollection.Current.Resolve<BrowserService>();
    public NavigationService Navigation => ServicesCollection.Current.Resolve<NavigationService>();
    public JavaScriptService JavaScript => ServicesCollection.Current.Resolve<JavaScriptService>();
    //public InteractionsService Interactions => ServicesCollection.Current.Resolve<InteractionsService>();
    public CookiesService Cookies => ServicesCollection.Current.Resolve<CookiesService>();
    public ComponentCreateService Components => ServicesCollection.Current.Resolve<ComponentCreateService>();
    public IAssert Assert => ServicesCollection.Current.Resolve<IAssert>();
    public WebElementService WebElement => ServicesCollection.Current.Resolve<WebElementService>();

    public void AddWebDriverOptions<TDriverOptions>(TDriverOptions options) where TDriverOptions : DriverOptions
    {
        string fullClassName = DetermineTestClassFullNameAttributes();
        ServicesCollection.Current.RegisterInstance(options, fullClassName);
    }

    public void AddWebDriverBrowserProfile<TBrowserProfile>(TBrowserProfile profile) where TBrowserProfile : class
    {
        string fullClassName = DetermineTestClassFullNameAttributes();
        ServicesCollection.Current.RegisterInstance(profile, fullClassName);
    }

    public void AddAdditionalCapability(string name, object value)
    {
        string fullClassName = DetermineTestClassFullNameAttributes();
        var dictionary = ServicesCollection.Current.Resolve<Dictionary<string, object>>($"caps-{fullClassName}") ?? new Dictionary<string, object>();
        dictionary.Add(name, value);
        ServicesCollection.Current.RegisterInstance(dictionary, $"caps-{fullClassName}");
    }

    public void AddElementEventHandler<TComponentsEventHandler>() where TComponentsEventHandler : ComponentEventHandlers
    {
        var elementEventHandler = (TComponentsEventHandler)Activator.CreateInstance(typeof(TComponentsEventHandler));
        elementEventHandler.SubscribeToAll();
    }

    public void RemoveElementEventHandler<TComponentsEventHandler>() where TComponentsEventHandler : ComponentEventHandlers
    {
        var elementEventHandler = (TComponentsEventHandler)Activator.CreateInstance(typeof(TComponentsEventHandler));
        elementEventHandler.UnsubscribeToAll();
    }

    public void AddPlugin<TExecutionExtension>() where TExecutionExtension : Plugin
    {
        ServicesCollection.Current.RegisterType<Plugin, TExecutionExtension>(Guid.NewGuid().ToString());
    }

    public void Initialize()
    {
        //var proxyService = new ProxyService();
        //ServicesCollection.Current.RegisterInstance(proxyService);
        //proxyService?.Start();
    }

    public void Dispose()
    {
        DisposeDriverService.DisposeAll();
        DisposeDriverService.Dispose();
        GC.SuppressFinalize(this);
    }

    public TPage Create<TPage>() where TPage : WebPage
    {
        TPage page = ServicesCollection.Current.Resolve<TPage>();
        return page;
    }

    public TPage GoTo<TPage>() where TPage : WebPage
    {
        TPage page = ServicesCollection.Current.Resolve<TPage>();
        page.Open();
        return page;
    }

    private string DetermineTestClassFullNameAttributes()
    {
        string fullClassName = string.Empty;
        var callStackTrace = new StackTrace();
        var currentAssembly = GetType().Assembly;

        foreach (var frame in callStackTrace.GetFrames())
        {
            var frameMethodInfo = frame.GetMethod() as MethodInfo;
            if (!frameMethodInfo?.ReflectedType?.Assembly.Equals(currentAssembly) == true &&
                (frameMethodInfo.Name.Equals("TestsArrange") || frameMethodInfo.Name.Equals("ScenarioInitialize")))
            {
                fullClassName = frameMethodInfo.DeclaringType.FullName;
                break;
            }
        }

        return fullClassName;
    }
}


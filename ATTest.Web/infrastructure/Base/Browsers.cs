﻿using System.Runtime.InteropServices;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium.IE;
using OpenQA.Selenium;
using ATTest.Web;
using ATTest.Core.Settings;
using WebDriverManager.DriverConfigs.Impl;
using WebDriverManager.Helpers;
using WebDriverManager;

namespace ATTest.Core
{
    public interface IBrowser
    {
        IWebDriver Init(string HubUrl);
    }

    public class EdgeBrowser : IBrowser
    {
        EdgeOptions edgeOptions;

        #region Constructor
        public EdgeBrowser(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            //edgeOptions.BinaryLocation = @"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe";
            //edgeOptions.AddUserProfilePreference("download.default_directory", Path.Combine(DefaultPath.FullName, "TestEvidence"));
            //edgeOptions.AddUserProfilePreference("download.default_directory", InstanceTestEvidenceDir ?? FileVal.GetFullAttachmentFilePath("TestEvidence"));

            edgeOptions = executionConfiguration.DriverOptions;
            edgeOptions.PageLoadStrategy = PageLoadStrategy.Normal;
            edgeOptions.AddArguments("--log-level=3");
            edgeOptions.AddArguments("--ignore-certificate-errors");
            edgeOptions.AddArguments("--ignore-ssl-errors");
            edgeOptions.AddArguments("--no-sandbox");
            edgeOptions.AddArguments("--disable-dev-shm-usage");

            //if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled)
            //{
            //    edgeOptions.Proxy = webDriverProxy;
            //}

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                edgeOptions.AddArguments("--blink-settings=scriptEnabled=false");
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath != null)
            {
                string packedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load packed extension from path: {packedExtensionPath}");
                edgeOptions.AddExtension(ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath);
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath != null)
            {
                string unpackedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load unpacked extension from path: {unpackedExtensionPath}");
                edgeOptions.AddArguments($"load-extension={unpackedExtensionPath}");
            }

            edgeOptions.SetLoggingPreference(LogType.Browser, LogLevel.Severe);
            edgeOptions.SetLoggingPreference("performance", LogLevel.All);
        }
        #endregion

        #region Init Edge driver
        public IWebDriver Init(string HubUrl = null)
        {
            if (string.IsNullOrEmpty(HubUrl))
            {
                return new EdgeDriver(edgeOptions);
            }

            return new RemoteWebDriver(
                new Uri(HubUrl),
                edgeOptions.ToCapabilities(),
                TimeSpan.FromMinutes(2)
            );
        }
        #endregion
    }

    public class FirefoxBrowser : IBrowser
    {
        FirefoxOptions firefoxOptions;
        FirefoxDriverService firefoxService;

        public FirefoxBrowser(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            new DriverManager().SetUpDriver(new FirefoxConfig(), VersionResolveStrategy.Latest);

            firefoxOptions = executionConfiguration.DriverOptions;
            firefoxOptions.PageLoadStrategy = PageLoadStrategy.Normal;
            firefoxOptions.AddAdditionalOption("acceptInsecureCerts", true);
            firefoxOptions.SetPreference("browser.download.folderList", 2);

            //if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled)
            //{
            //    firefoxOptions.Proxy = webDriverProxy;
            //}

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                firefoxOptions.SetPreference("pdfjs.disabled", true);
            }

            firefoxService = FirefoxDriverService.CreateDefaultService();
            firefoxService.SuppressInitialDiagnosticInformation = true;
            //firefoxService.Port = GetFreeTcpPort();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                if (File.Exists(@"C:\Program Files\Mozilla Firefox\firefox.exe"))
                {
                    firefoxService.FirefoxBinaryPath = @"C:\Program Files\Mozilla Firefox\firefox.exe";
                }
                else
                {
                    firefoxService.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                }
            }

            var firefoxProfile = ServicesCollection.Current.Resolve<FirefoxProfile>(executionConfiguration.ClassFullName);
            if (firefoxProfile == null)
            {
                firefoxOptions.Profile = new FirefoxProfile(executionConfiguration.GetDriverExecutablePath());
                ServicesCollection.Current.RegisterInstance(firefoxOptions.Profile, executionConfiguration.ClassFullName);
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath != null)
            {
                Logger.LogError($"Packed Extension loading not supported in Firefox!");
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath != null)
            {
                Logger.LogError($"Unpacked Extension loading not supported in Firefox!");
            }
        }

        public IWebDriver Init(string HubUrl = null)
        {
            if (string.IsNullOrEmpty(HubUrl))
            {
                var firefoxTimeout = TimeSpan.FromSeconds(180);
                return new FirefoxDriver(
                    firefoxService,
                    firefoxOptions,
                    firefoxTimeout
                );
            }

            if (firefoxOptions == null)
            {
                firefoxOptions = new FirefoxOptions();
            }

            return new RemoteWebDriver
                (new Uri(HubUrl),
                firefoxOptions.ToCapabilities(),
                TimeSpan.FromMinutes(2)
            );
        }
    }

    public class ChromeBrowser : IBrowser
    {
        ChromeOptions chromeOptions;
        ChromeDriverService chromeDriverService;

        public ChromeBrowser(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            new DriverManager().SetUpDriver(new ChromeConfig(), VersionResolveStrategy.MatchingBrowser);
            chromeOptions = executionConfiguration.DriverOptions;
            chromeOptions.AddArguments("--log-level=3");

            //DebuggerPort = GetFreeTcpPort();

            //if (executionConfiguration.IsLighthouseEnabled)
            //{
            //    chromeOptions.AddArgument("--remote-debugging-address=0.0.0.0");
            //    chromeOptions.AddArgument($"--remote-debugging-port={DebuggerPort}");
            //    ////ProcessProvider.StartCLIProcess($"chrome-debug --port={Port}");
            //    ////chromeOptions.DebuggerAddress = $"127.0.0.1:{Port}";
            //}

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath != null)
            {
                string packedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load packed extension from path: {packedExtensionPath}");
                chromeOptions.AddExtension(ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath);
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath != null)
            {
                string unpackedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load unpacked extension from path: {unpackedExtensionPath}");
                chromeOptions.AddArguments($"load-extension={unpackedExtensionPath}");
            }

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                chromeOptions.AddArguments("--blink-settings=scriptEnabled=false");
            }

            //if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled && !executionConfiguration.IsLighthouseEnabled)
            //{
            //    chromeOptions.Proxy = webDriverProxy;
            //}

            chromeOptions.AddArgument("hide-scrollbars");
            chromeOptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
            chromeOptions.SetLoggingPreference("performance", LogLevel.All);
        }
           
        public IWebDriver Init(string HubUrl = null)
        {
            if (string.IsNullOrEmpty(HubUrl))
            {
                var chromeDriverService = ChromeDriverService.CreateDefaultService();

                ServicesCollection.Current.RegisterInstance<ChromeDriverService>(chromeDriverService);

                chromeDriverService.SuppressInitialDiagnosticInformation = true;
                chromeDriverService.EnableVerboseLogging = false;

                //Port = GetFreeTcpPort();
                //chromeDriverService.Port = Port;

                return new ChromeDriver(
                    chromeDriverService,
                    chromeOptions,
                    TimeSpan.FromMinutes(3)
                );
            }

            RemoteWebDriver remoteWebDriver = new RemoteWebDriver(
                new Uri(HubUrl),
                chromeOptions.ToCapabilities(),
                TimeSpan.FromMinutes(3)
            );
            remoteWebDriver.Manage().Timeouts().PageLoad.Add(TimeSpan.FromMinutes(3));
            return remoteWebDriver;
        }
    }

    public class ChromeBrowserHeadless : IBrowser
    {
        ChromeOptions chromeHeadlessOptions;
        ChromeDriverService chromeHeadlessDriverService;

        public ChromeBrowserHeadless(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            new DriverManager().SetUpDriver(new ChromeConfig(), VersionResolveStrategy.MatchingBrowser);
            var chromeHeadlessOptions = executionConfiguration.DriverOptions;
            chromeHeadlessOptions.AddArguments("--headless");
            chromeHeadlessOptions.AddArguments("--log-level=3");

            chromeHeadlessOptions.AddArguments("--test-type");
            chromeHeadlessOptions.AddArguments("--disable-infobars");
            chromeHeadlessOptions.AddArguments("--allow-no-sandbox-job");
            chromeHeadlessOptions.AddArguments("--ignore-certificate-errors");
            chromeHeadlessOptions.AddArguments("--disable-gpu");
            chromeHeadlessOptions.AddArguments("--no-sandbox");
            chromeHeadlessOptions.AddUserProfilePreference("credentials_enable_service", false);
            chromeHeadlessOptions.AddUserProfilePreference("profile.password_manager_enabled", false);
            chromeHeadlessOptions.AddArgument("hide-scrollbars");
            chromeHeadlessOptions.UnhandledPromptBehavior = UnhandledPromptBehavior.Dismiss;

            //if (executionConfiguration.IsLighthouseEnabled)
            //{
            //    chromeHeadlessOptions.AddArgument("--remote-debugging-address=0.0.0.0");
            //    chromeHeadlessOptions.AddArgument($"--remote-debugging-port={DebuggerPort}");
            //}

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath != null)
            {
                string packedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load packed extension from path: {packedExtensionPath}");
                chromeHeadlessOptions.AddExtension(ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath);
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath != null)
            {
                string unpackedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load unpacked extension from path: {unpackedExtensionPath}");
                chromeHeadlessOptions.AddArguments($"load-extension={unpackedExtensionPath}");
            }

            //if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled)
            //{
            //    chromeHeadlessOptions.Proxy = webDriverProxy;
            //}

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                chromeHeadlessOptions.AddArguments("--blink-settings=scriptEnabled=false");
            }

            chromeHeadlessOptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
            chromeHeadlessOptions.SetLoggingPreference("performance", LogLevel.All);
        }

        public IWebDriver Init(string HubUrl = null)
        {
            chromeHeadlessDriverService = ChromeDriverService.CreateDefaultService();
            chromeHeadlessDriverService.SuppressInitialDiagnosticInformation = true;
            //Port = GetFreeTcpPort();
            //chromeHeadlessDriverService.Port = Port;
            
            //Port = GetFreeTcpPort();
            //chromeHeadlessDriverService.Port = Port;
            //DebuggerPort = GetFreeTcpPort();

            if (string.IsNullOrEmpty(HubUrl))
            {
                ChromeDriver chromeDriver = new ChromeDriver(chromeHeadlessDriverService, chromeHeadlessOptions, TimeSpan.FromMinutes(3));
                chromeDriver.Manage().Timeouts().PageLoad.Add(TimeSpan.FromMinutes(3));
                chromeDriver.Manage().Timeouts().ImplicitWait.Add(TimeSpan.FromSeconds(30));
                return chromeDriver;
            }

            RemoteWebDriver remoteWebDriver = new RemoteWebDriver(
                new Uri(HubUrl),
                chromeHeadlessOptions.ToCapabilities(),
                TimeSpan.FromMinutes(3)
            );
            remoteWebDriver.Manage().Timeouts().PageLoad.Add(TimeSpan.FromMinutes(3));
            return remoteWebDriver;
        }
    }

    public class FirefoxBrowserHeadless : IBrowser
    {
        FirefoxOptions firefoxHeadlessOptions;
        FirefoxDriverService firefoxService;

        public FirefoxBrowserHeadless(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            new DriverManager().SetUpDriver(new FirefoxConfig());
            var firefoxHeadlessOptions = executionConfiguration.DriverOptions;
            firefoxHeadlessOptions.AddArguments("--headless");
            firefoxHeadlessOptions.AddAdditionalOption("acceptInsecureCerts", true);

            // if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled)
            // {
            //     firefoxHeadlessOptions.Proxy = webDriverProxy;
            // }

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                firefoxHeadlessOptions.addPreference("javascript.enabled", false);
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath != null)
            {
                string packedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load packed extension from path: {packedExtensionPath}");
                firefoxHeadlessOptions.Profile.AddExtension(ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath);
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath != null)
            {
                Logger.LogError($"Unpacked Extension loading not supported in Firefox!");
            }

            firefoxService = FirefoxDriverService.CreateDefaultService();
            firefoxService.SuppressInitialDiagnosticInformation = true;

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                if (File.Exists(@"C:\Program Files\Mozilla Firefox\firefox.exe"))
                {
                    firefoxService.FirefoxBinaryPath = @"C:\Program Files\Mozilla Firefox\firefox.exe";
                }
                else
                {
                    firefoxService.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                }
            }

            // firefoxService.Port = GetFreeTcpPort();
        }

        public IWebDriver Init(string HubUrl = null) 
        {
            if (string.IsNullOrEmpty(HubUrl))
            {
                return new FirefoxDriver(
                    firefoxService,
                    firefoxHeadlessOptions
                );
            }

            if (firefoxHeadlessOptions == null)
            {
                firefoxHeadlessOptions = new FirefoxOptions();
            }

            return new RemoteWebDriver
                (new Uri(HubUrl),
                firefoxHeadlessOptions.ToCapabilities(),
                TimeSpan.FromMinutes(2)
            );
        }
    }

    public class EdgeBrowserHeadless: IBrowser
    {
        EdgeOptions edgeHeadlessOptions;
        public EdgeBrowserHeadless(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            edgeHeadlessOptions = (EdgeOptions)executionConfiguration.DriverOptions;
            edgeHeadlessOptions.AddArguments("--headless");
            edgeHeadlessOptions.AddArguments("--log-level=3");

            edgeHeadlessOptions.AddArguments("--test-type");
            edgeHeadlessOptions.AddArguments("--disable-infobars");
            edgeHeadlessOptions.AddArguments("--allow-no-sandbox-job");
            edgeHeadlessOptions.AddArguments("--ignore-certificate-errors");
            edgeHeadlessOptions.AddArguments("--disable-gpu");
            edgeHeadlessOptions.AddArguments("--no-sandbox");
            edgeHeadlessOptions.AddUserProfilePreference("credentials_enable_service", false);
            edgeHeadlessOptions.AddUserProfilePreference("profile.password_manager_enabled", false);
            edgeHeadlessOptions.AddArgument("hide-scrollbars");
            edgeHeadlessOptions.UnhandledPromptBehavior = UnhandledPromptBehavior.Dismiss;

            edgeHeadlessOptions.PageLoadStrategy = PageLoadStrategy.Normal;
            // if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled)
            // {
            //     edgeHeadlessOptions.Proxy = webDriverProxy;
            // }

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                edgeHeadlessOptions.AddArguments("--blink-settings=scriptEnabled=false");
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath != null)
            {
                string packedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load packed extension from path: {packedExtensionPath}");
                edgeHeadlessOptions.AddExtension(ConfigurationService.GetSection<WebSettings>().ExecutionSettings.PackedExtensionPath);
            }

            if (ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath != null)
            {
                string unpackedExtensionPath = ConfigurationService.GetSection<WebSettings>().ExecutionSettings.UnpackedExtensionPath.NormalizeAppPath();
                Logger.LogInformation($"Trying to load unpacked extension from path: {unpackedExtensionPath}");
                edgeHeadlessOptions.AddExtension(unpackedExtensionPath);
            }

            edgeHeadlessOptions.SetLoggingPreference(LogType.Browser, LogLevel.Severe);
            edgeHeadlessOptions.SetLoggingPreference("performance", LogLevel.All);
        }

        public IWebDriver Init(string HubUrl = null)
        {
            return new EdgeDriver(edgeHeadlessOptions);
        }
    }

    public class InternetExplorerBrowser : IBrowser
    {
        InternetExplorerOptions ieOptions;
        string _driverExecutablePath;

        public InternetExplorerBrowser(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            new DriverManager().SetUpDriver(new InternetExplorerConfig());
            var ieOptions = executionConfiguration.DriverOptions;
            ieOptions.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            ieOptions.IgnoreZoomLevel = true;
            ieOptions.EnableNativeEvents = false;
            ieOptions.EnsureCleanSession = true;
            ieOptions.PageLoadStrategy = PageLoadStrategy.Eager;
            ieOptions.ForceShellWindowsApi = true;
            ieOptions.AddAdditionalCapability("disable-popup-blocking", true);

            // if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled)
            // {
            //     ieOptions.Proxy = webDriverProxy;
            // }

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                throw new NotSupportedException("disable javascript not suported for Safari and InternetExplorer.");
            }

            _driverExecutablePath = executionConfiguration.GetDriverExecutablePath();
        }

        public IWebDriver Init(string HubUrl = null)
        {
            return new InternetExplorerDriver(_driverExecutablePath, ieOptions);
        }
    }

    public class SafariBrowser : IBrowser
    {
        SafariOptions safariOptions;
        public SafariBrowser(BrowserConfiguration executionConfiguration, string InstanceTestEvidenceDir = null)
        {
            safariOptions = executionConfiguration.DriverOptions;

            // if (executionConfiguration.ShouldCaptureHttpTraffic && _proxyService.IsEnabled)
            // {
            //     safariOptions.Proxy = webDriverProxy;
            // }

            if (executionConfiguration.ShouldDisableJavaScript)
            {
                throw new NotSupportedException("disable javascript not suported for Safari and InternetExplorer.");
            }
        }

        public IWebDriver Init(string HubUrl = null)
        {
            return new SafariDriver(safariOptions);
        }
    }
}
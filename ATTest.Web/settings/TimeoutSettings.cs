﻿
namespace ATTest.Web
{
    public class TimeoutSettings
    {
        public int PageLoadTimeout { get; set; } = 30000;
        public int ScriptTimeout { get; set; } = 30000;
        public int WaitUntilReadyTimeout { get; set; } = 30000;
        public int WaitForJavaScriptAnimationsTimeout { get; set; } = 30000;
        public int WaitForAngularTimeout { get; set; } = 30000;
        public int WaitForPartialUrl { get; set; } = 30000;
        public int ValidationsTimeout { get; set; } = 30000;

        public int WaitForAjaxTimeout { get; set; } = 30000;
        public int SleepInterval { get; set; } = 1000;

        public int ElementToBeVisibleTimeout { get; set; } = 30000;
        public int ElementToExistTimeout { get; set; } = 30000;
        public int ElementToNotExistTimeout { get; set; } = 10000;
        public int ElementToBeClickableTimeout { get; set; } = 30000;
        public int ElementNotToBeVisibleTimeout { get; set; } = 10000;
        public int ElementToHaveContentTimeout { get; set; } = 30000;
    }
}

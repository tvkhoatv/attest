﻿using System;
using System.Drawing;
using ATTest.Core.Utilities;
using ATTest.Web.Enums;
using OpenQA.Selenium;

namespace ATTest.Web
{
    public class BrowserConfiguration : IEquatable<BrowserConfiguration>
    {
        static readonly string _driverExecutablePath = ExecutionDirectoryResolver.GetDriverExecutablePath();

        public BrowserConfiguration()
        {
        }

        public BrowserConfiguration(
            ExecutionType executionType,
            Lifecycle browserBehavior,
            BrowserType browserType,
            Size size,
            string classFullName,
            bool shouldCaptureHttpTraffic,
            bool shouldDisableJavaScript,
            bool shouldAutomaticallyScrollToVisible,
            DriverOptions driverOptions = null)
        : this(browserBehavior, browserType, size, shouldCaptureHttpTraffic, shouldDisableJavaScript, shouldAutomaticallyScrollToVisible)
        {
            ExecutionType = executionType;
            ClassFullName = classFullName;
            DriverOptions = driverOptions;
        }

        public BrowserConfiguration(
            Lifecycle browserBehavior,
            BrowserType browserType,
            Size size, bool shouldCaptureHttpTraffic,
            bool shouldDisableJavaScript,
            bool shouldAutomaticallyScrollToVisible)
        : this(browserType, shouldCaptureHttpTraffic, shouldDisableJavaScript, shouldAutomaticallyScrollToVisible)
        {
            BrowserBehavior = browserBehavior;
            Size = size;
        }

        public BrowserConfiguration(
            BrowserType browserType, 
            bool shouldCaptureHttpTraffic, 
            bool shouldDisableJavaScript, 
            bool shouldAutomaticallyScrollToVisible)
        {
            BrowserType = browserType;
            ShouldCaptureHttpTraffic = shouldCaptureHttpTraffic;
            ShouldDisableJavaScript = shouldDisableJavaScript;
            ShouldAutomaticallyScrollToVisible = shouldAutomaticallyScrollToVisible;
        }

        public BrowserType BrowserType { get; set; } = BrowserType.Chrome;

        public Lifecycle BrowserBehavior { get; set; } = Lifecycle.RestartEveryTime;

        public Size Size { get; set; }

        public bool ShouldCaptureHttpTraffic { get; set; }
        public bool ShouldDisableJavaScript { get; set; }

        public string ClassFullName { get; set; }

        public dynamic DriverOptions { get; set; }

        public ExecutionType ExecutionType { get; set; } = ExecutionType.Regular;

        public bool ShouldAutomaticallyScrollToVisible { get; set; }
        public bool IsLighthouseEnabled { get; set; }

        public bool Equals(BrowserConfiguration other) => ExecutionType.Equals(other?.ExecutionType) &&
                                                          BrowserType.Equals(other?.BrowserType) &&
                                                          ShouldCaptureHttpTraffic.Equals(other?.ShouldCaptureHttpTraffic) &&
                                                          ShouldDisableJavaScript.Equals(other?.ShouldDisableJavaScript) &&
                                                          Size.Equals(other?.Size) &&
                                                          IsLighthouseEnabled.Equals(other?.IsLighthouseEnabled) &&
                                                          ShouldAutomaticallyScrollToVisible.Equals(other?.ShouldAutomaticallyScrollToVisible);

        public override bool Equals(object obj)
        {
            var browserConfiguration = (BrowserConfiguration)obj;
            return ExecutionType.Equals(browserConfiguration?.ExecutionType) &&
            BrowserType.Equals(browserConfiguration?.BrowserType) &&
            ShouldCaptureHttpTraffic.Equals(browserConfiguration?.ShouldCaptureHttpTraffic) &&
            ShouldDisableJavaScript.Equals(browserConfiguration?.ShouldDisableJavaScript) &&
            Size.Equals(browserConfiguration?.Size) &&
            ShouldAutomaticallyScrollToVisible.Equals(browserConfiguration?.ShouldAutomaticallyScrollToVisible) &&
            IsLighthouseEnabled.Equals(browserConfiguration?.IsLighthouseEnabled);
        }

        public override int GetHashCode()
        {
            return ExecutionType.GetHashCode() +
                    BrowserType.GetHashCode() +
                    ShouldCaptureHttpTraffic.GetHashCode() +
                    ShouldDisableJavaScript.GetHashCode() +
                    Size.GetHashCode() +
                    IsLighthouseEnabled.GetHashCode() +
                    ShouldAutomaticallyScrollToVisible.GetHashCode();
        }

        public string GetDriverExecutablePath()
        {
            return _driverExecutablePath;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATTest.Web
{
    public class ExecutionSettings
    {
        public string ExecutionType { get; set; }
        public string DefaultBrowser { get; set; }
        public string BrowserVersion { get; set; }
        public string DefaultLifeCycle { get; set; }
        public string Resolution { get; set; }
        public string Url { get; set; }
        public string FileRemoteLocation { get; set; }
        public List<Dictionary<string, string>> Arguments { get; set; }
        public string PackedExtensionPath { get; set; }
        public string UnpackedExtensionPath { get; set; }
        public bool IsCloudRun { get; set; }
    }
}

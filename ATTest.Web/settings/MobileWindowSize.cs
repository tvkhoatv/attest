﻿namespace ATTest.Web
{
    public enum MobileWindowSize
    {
        _360_640 = 101,
        _375_667 = 102,
        _720_1280 = 103,
        _320_568 = 104,
        _414_736 = 105,
        _320_534 = 106,
    }
}
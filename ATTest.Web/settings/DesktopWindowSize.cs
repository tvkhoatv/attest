﻿namespace ATTest.Web
{
    public enum DesktopWindowSize
    {
        _1366_768 = 1,
        _1920_1080 = 2,
        _1440_900 = 3,
        _1600_900 = 4,
        _1280_800 = 5,
        _1280_1024 = 6,
    }
}
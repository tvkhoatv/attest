﻿using System;

namespace ATTest.Web.Events
{
    public class UrlNotNavigatedEventArgs
    {
        public UrlNotNavigatedEventArgs(Exception exp) => Exception = exp;

        public Exception Exception { get; }
    }
}

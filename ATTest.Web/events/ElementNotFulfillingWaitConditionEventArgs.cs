﻿using System;

namespace ATTest.Web.Events
{
    public class ElementNotFulfillingWaitConditionEventArgs
    {
        public ElementNotFulfillingWaitConditionEventArgs(Exception exp) => Exception = exp;

        public Exception Exception { get; }
    }
}

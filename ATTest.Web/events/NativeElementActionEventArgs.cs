﻿using OpenQA.Selenium;

namespace ATTest.Web.Events
{
    public class NativeElementActionEventArgs
    {
        public NativeElementActionEventArgs(IWebElement element) => Element = element;

        public IWebElement Element { get; }
    }
}

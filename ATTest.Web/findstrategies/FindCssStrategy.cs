﻿using OpenQA.Selenium;

namespace ATTest.Web.Locators
{
    public class FindCssStrategy : FindStrategy
    {
        public FindCssStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.CssSelector(Value);
        }

        public override string ToString()
        {
            return $"CSS = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.CssSelector(Value));
        }
    }
}

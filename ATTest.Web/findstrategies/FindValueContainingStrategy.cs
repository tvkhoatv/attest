﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindValueContainingStrategy : FindStrategy
    {
        public FindValueContainingStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.CssSelector($"[value*='{Value}']");
        }

        public override string ToString()
        {
            return $"Value containing {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.CssSelector($"[value*='{Value}']"));
        }
    }
}

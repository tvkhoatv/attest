﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindIdEndingWithStrategy : FindStrategy
    {
        public FindIdEndingWithStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.CssSelector($"[id$='{Value}']");
        }

        public override string ToString()
        {
            return $"ID ending with {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.CssSelector($"[id$='{Value}']"));
        }
    }
}

﻿using OpenQA.Selenium;

namespace ATTest.Web;

//public abstract class FindStrategy<T> where T : FindStrategy<T>
public abstract class FindStrategy
{
    //private static readonly Lazy<T> Lazy = new(() => (Activator.CreateInstance(typeof(T), true) as T)!);
    //public static T Instance => Lazy.Value;

    public FindStrategy(string value) => Value = value;

    public string Value { get; }

    public abstract OpenQA.Selenium.By Convert();

    public abstract OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver);
}


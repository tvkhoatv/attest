﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindAttributeContainingStrategy : FindStrategy
    {
        private readonly string _attributeName;
        public FindAttributeContainingStrategy(string attributeName, string value) : base(value)
        {
            _attributeName = attributeName;
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.CssSelector($"[{_attributeName}*='{Value}']");
        }

        public override string ToString()
        {
            return $"{_attributeName} = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.CssSelector($"[{_attributeName}*='{Value}']"));
        }
    }
}

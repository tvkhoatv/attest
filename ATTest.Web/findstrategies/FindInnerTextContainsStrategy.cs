﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindInnerTextContainsStrategy : FindStrategy
    {
        public FindInnerTextContainsStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.XPath($"//*[contains(text(), '{Value}')]");
        }

        public override string ToString()
        {
            return $"InnerText containing {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.XPath($"//*[contains(text(), '{Value}')]"));
        }
    }
}

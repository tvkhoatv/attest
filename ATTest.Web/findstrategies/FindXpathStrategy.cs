﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindXpathStrategy : FindStrategy
    {
        public FindXpathStrategy(string value) : base(value)
        {
        }

        public override By Convert()
        {
            return By.XPath(Value);
        }

        public override string ToString()
        {
            return $"XPath = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.XPath(Value));
        }
    }
}

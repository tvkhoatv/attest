﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindNameStrategy : FindStrategy
    {
        public FindNameStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.Name(Value);
        }

        public override string ToString()
        {
            return $"Name = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.Name(Value));
        }
    }
}

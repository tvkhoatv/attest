﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindTagStrategy : FindStrategy
    {
        public FindTagStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.TagName(Value);
        }

        public override string ToString()
        {
            return $"Tag = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.TagName(Value));
        }
    }
}

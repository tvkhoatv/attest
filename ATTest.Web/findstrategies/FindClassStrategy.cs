﻿using AngleSharp.Dom;
using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindClassStrategy : FindStrategy
    {
        public FindClassStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.ClassName(Value);
        }

        public override string ToString()
        {
            return $"Class = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.ClassName(Value));
        }
    }
}

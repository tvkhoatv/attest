﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindLinkTextStrategy : FindStrategy
    {
        public FindLinkTextStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.LinkText(Value);
        }

        public override string ToString()
        {
            return $"LinkText = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.LinkText(Value));
        }
    }
}

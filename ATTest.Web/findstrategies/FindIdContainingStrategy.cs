﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindIdContainingStrategy : FindStrategy
    {
        public FindIdContainingStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.CssSelector($"[id*='{Value}']");
        }

        public override string ToString()
        {
            return $"ID containing {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.CssSelector($"[id*='{Value}']"));
        }
    }
}

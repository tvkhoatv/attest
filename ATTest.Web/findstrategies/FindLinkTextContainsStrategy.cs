﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindLinkTextContainsStrategy : FindStrategy
    {
        public FindLinkTextContainsStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.XPath($"//a[contains(text(), '{Value}')]");
        }

        public override string ToString()
        {
            return $"LinkText containing {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.XPath($"//a[contains(text(), '{Value}')]"));
        }
    }
}

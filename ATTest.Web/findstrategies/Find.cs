﻿namespace ATTest.Web
{
    public sealed class Find
    {
        static Find() => By = new FindStrategyFactory();

        public static FindStrategyFactory By { get; }
    }
}

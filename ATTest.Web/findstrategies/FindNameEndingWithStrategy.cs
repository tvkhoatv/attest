﻿using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindNameEndingWithStrategy : FindStrategy
    {
        public FindNameEndingWithStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.CssSelector($"[name$='{Value}']");
        }

        public override string ToString()
        {
            return $"Name ending with {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.CssSelector($"[name$='{Value}']"));
        }
    }
}

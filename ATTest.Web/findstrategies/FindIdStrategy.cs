﻿using OpenQA.Selenium;

namespace ATTest.Web.Locators
{
    public class FindIdStrategy : FindStrategy
    {
        public FindIdStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.Id(Value);
        }

        public override string ToString()
        {
            return $"ID = {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.Id(Value));
        }
    }
}

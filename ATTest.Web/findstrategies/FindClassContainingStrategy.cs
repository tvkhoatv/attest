﻿using AngleSharp.Dom;
using OpenQA.Selenium;

namespace ATTest.Web
{
    public class FindClassContainingStrategy: FindStrategy
    {
        public FindClassContainingStrategy(string value) : base(value)
        {
        }

        public override OpenQA.Selenium.By Convert()
        {
            return OpenQA.Selenium.By.CssSelector($"[class*='{Value}']");
        }

        public override string ToString()
        {
            return $"Class containing {Value}";
        }

        public override OpenQA.Selenium.IWebElement Find(IWebDriver wrappedDriver)
        {
            return wrappedDriver.FindElement(OpenQA.Selenium.By.CssSelector($"[class*='{Value}']"));
        }
    }
}

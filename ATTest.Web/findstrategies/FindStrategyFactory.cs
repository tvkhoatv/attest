﻿using ATTest.Web.Locators;

namespace ATTest.Web
{
    public class FindStrategyFactory
    {
        public FindIdStrategy Id(string id) => new FindIdStrategy(id);

        public FindXpathStrategy Xpath(string xpath) => new FindXpathStrategy(xpath);

        public FindCssStrategy CssClass(string css) => new FindCssStrategy(css);

        public FindLinkTextStrategy LinkText(string linkText) => new FindLinkTextStrategy(linkText);

        public FindNameStrategy Name(string name) => new FindNameStrategy(name);

        public FindTagStrategy Tag(string tag) => new FindTagStrategy(tag);

        public FindStrategy CssClassContaining(string cssClass) => new FindClassContainingStrategy(cssClass);

        public FindIdContainingStrategy IdContaining(string id) => new FindIdContainingStrategy(id);

        public FindIdEndingWithStrategy IdEndingWith(string id) => new FindIdEndingWithStrategy(id);

        public FindInnerTextContainsStrategy InnerTextContains(string text) => new FindInnerTextContainsStrategy(text);

        public FindLinkTextContainsStrategy LinkTextContains(string text) => new FindLinkTextContainsStrategy(text);

        public FindValueContainingStrategy ValueEndingWith(string value) => new FindValueContainingStrategy(value);

        public FindAttributeContainingStrategy AttributeContaining(string attributeName, string value) => new FindAttributeContainingStrategy(attributeName, value);
    }
}

﻿namespace ATTest.Web.Enums
{
    public enum ExecutionType
    {
        Regular,
        BrowserStack,
        CrossBrowserTesting,
        Grid,
        SauceLabs,
    }
}

﻿namespace ATTest.Web
{
    public enum ToastNotificationType
    {
        Information,
        Success,
        Warning,
        Error,
    }
}

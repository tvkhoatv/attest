﻿namespace ATTest.Web;

public enum BrowserType
{
    NotSet,
    Chrome,
    ChromeHeadless,
    Firefox,
    FirefoxHeadless,
    InternetExplorer,
    Edge,
    EdgeHeadless,
    Opera,
    Safari,
}
﻿using ATTest.TestCase.Common;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace ATTest.TestCase.PageObjects.Web
{
    public class LoginPage : BaseObject
    {
        public override string Url => "/us/Account/Logon";

        #region Declare elements
        [FindsBy(How = How.Id, Using = "accountMenu")]
        protected IWebElement? Lbl_AccountMenu { get; set; }

        [FindsBy(How = How.Id, Using = "UserName")]
        protected IWebElement? Txt_username { get; set; }

        [FindsBy(How = How.Id, Using = "Password")]
        protected IWebElement? Txt_password { get; set; }

        [FindsBy(How = How.Id, Using = "loginButton")]
        protected IWebElement? Btn_login { get; set; }

        [FindsBy(How = How.Id, Using = "signUp")]
        protected IWebElement? Btn_CreateAnAccount { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Forgot Password')]")]
        protected IWebElement? Btn_ForgotPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'dashboard-container')]")]
        protected IWebElement? Lbl_DashboardContainer { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name='UserName']/following-sibling::*[@class='error']")]
        protected IWebElement? Lbl_UsernameError { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name='Password']/following-sibling::*[@class='error']")]
        protected IWebElement? Lbl_PasswordError { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'logonErrors')]")]
        protected IWebElement? Lbl_LoginError { get; set; }

        [FindsBy(How = How.Id, Using = "sendInstructionButton")]
        protected IWebElement? Btn_SendIntructions { get; set; }

        [FindsBy(How = How.XPath, Using = "//script[contains(text(),'window.intercomSettings')]")]
        protected IWebElement? Lbl_Intercom { get; set; }

        [FindsBy(How = How.Name, Using = "__RequestVerificationToken")]
        protected IWebElement? Txt_RequestVerificationToken { get; set; }
        #endregion

        #region Sign on
        //public LoginPage SignOn(Credential credential)
        //{
        //    TakeActions(() =>
        //    {
        //        keywords.SendkeysOnTextBox(Txt_username, credential?.EmailAddress);
        //        keywords.SendkeysOnTextBox(Txt_password, credential?.Password);
        //        keywords.JSClickOn(Btn_login);
        //    });
        //    return this;
        //}

        public LoginPage SignOn()
        {
            Txt_username?.SendKeys("khoat@tracersystems.co.uk");
            Txt_password?.SendKeys("Abcd@1234");

            Btn_login?.Click();

            return this;
        }
        #endregion
    }
}

﻿using ATTest.TestCase.Common;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace ATTest.TestCase.PageObjects.Web
{
    public class CustomerPOM : BaseObject
    {
        public override string Url => "/us/Customer";

        #region Declare elements
        [FindsBy(How = How.Id, Using = "CustomerName")]
        protected IWebElement? Txt_Name { get; set; }

        [FindsBy(How = How.Name, Using = "AccountNumber")]
        protected IWebElement? Txt_AccountNumber { get; set; }

        [FindsBy(How = How.Name, Using = "CustomerTypeId_input")]
        protected IWebElement? Txt_CustomerType { get; set; }

        [FindsBy(How = How.Name, Using = "SellingRateId_input")]
        protected IWebElement? Txt_CustomerSellingRate { get; set; }

        [FindsBy(How = How.Id, Using = "ContactFirstName")]
        protected IWebElement? Txt_ContactFirstName { get; set; }

        [FindsBy(How = How.Id, Using = "ContactLastName")]
        protected IWebElement? Txt_ContactLastName { get; set; }

        [FindsBy(How = How.Id, Using = "ContactTelephone")]
        protected IWebElement? Txt_ContactTelephone { get; set; }

        [FindsBy(How = How.Id, Using = "ContactEmail")]
        protected IWebElement? Txt_ContactEmail { get; set; }

        [FindsBy(How = How.Id, Using = "ContactPosition")]
        protected IWebElement? Txt_ContactPosition { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@targettab,'contactsTab')]//button[contains(@class,'button-green')]")]
        protected IWebElement? Btn_AddContact { get; set; }

        [FindsBy(How = How.Id, Using = "createSiteButtonMenu")]
        protected IWebElement? Btn_AddSite { get; set; }

        [FindsBy(How = How.Id, Using = "createJobButtonMenu")]
        protected IWebElement? Btn_LogJob { get; set; }

        [FindsBy(How = How.Id, Using = "createQuoteButtonMenu")]
        protected IWebElement? Btn_LogQuote { get; set; }

        [FindsBy(How = How.Id, Using = "BillingName")]
        protected IWebElement? Txt_BillingName { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress1")]
        protected IWebElement? Txt_BillingAddress1 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress2")]
        protected IWebElement? Txt_BillingAddress2 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress3")]
        protected IWebElement? Txt_BillingAddress3 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAddress4")]
        protected IWebElement? Txt_BillingAddress4 { get; set; }

        [FindsBy(How = How.Id, Using = "BillingPostcode")]
        protected IWebElement? Txt_BillingPostCode { get; set; }

        [FindsBy(How = How.Id, Using = "BillingTelephone")]
        protected IWebElement? Txt_BillingTelephone { get; set; }

        [FindsBy(How = How.Name, Using = "UseBillingAddress")]
        protected IWebElement? Chk_EnableBilling { get; set; }

        [FindsBy(How = How.Name, Using = "EmailAddress")]
        protected IWebElement? Txt_BillingEmail { get; set; }

        [FindsBy(How = How.Id, Using = "BillingAccountNumber")]
        protected IWebElement? Txt_BillingAccountNumber { get; set; }

        [FindsBy(How = How.Name, Using = "VatNumber")]
        protected IWebElement? Txt_VatNumber { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@aria-describedby,'TagIds_taglist')]")]
        protected IWebElement? Txt_CustomerTag { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id ='TagIds_listbox']//li")]
        protected IWebElement? Lbl_CustomerTag { get; set; }

        [FindsBy(How = How.Name, Using = "Active")]
        protected IWebElement? Chk_ActiveInDetails { get; set; }

        [FindsBy(How = How.XPath, Using = "//label[contains(normalize-space(),'Active')]/input")]
        protected IWebElement? Chk_Active { get; set; }

        [FindsBy(How = How.Id, Using = "createCGroupInvoiceButtonMenu")]
        protected IWebElement? Btn_AddCGroupInvoice { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@class='additional-menu']//a[contains(@href,'/Customer/Create')]")]
        protected IWebElement? Btn_AddCustomer { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[span[.='Assign Schedule of Rates Library']]")]
        protected IWebElement? Btn_AssignSORLibrary { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='jl-switch-isshowportallink']/following-sibling::*[contains(@class,'v-switch-core')]")]
        protected IWebElement? Swt_ShowPortalLinkOnInvoice { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'jl-table-sticky sticky-right table-actions')]//*[contains(@class,'jl-icon-red')]//parent::button")]
        protected IList<IWebElement>? Btn_DeleteSORLibrary { get; set; }
        #endregion
    }
}

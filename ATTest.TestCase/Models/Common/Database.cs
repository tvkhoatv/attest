﻿namespace ATTest.TestCase.Models.Common
{
    public class MongoDb
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public string Collection { get; set; }
        public string Region { get; set; }
    }
}

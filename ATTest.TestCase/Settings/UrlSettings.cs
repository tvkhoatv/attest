﻿namespace ATTest.TestCase.Settings
{
    public sealed class UrlSettings
    {
        public string BaseUrl { get; set; } = "";
        public string PortalUrl { get; set; }
    }
}

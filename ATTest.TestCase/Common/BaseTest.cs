﻿using ATTest.Web;
using Environment = ATTest.TestCase.Models.Common.Environment;
using ATTest.TestCase.Models.Common;
using ATTest.Web.MSTest;

namespace ATTest.TestCase.Common
{
    [TestClass]
    public abstract partial class BaseTest : WebTest
    {
        #region Declare fields
        public TestContext TestContext { get; set; }

        public static Environment Environment;
        protected static IEnumerable<BrowserType> browserTypes { get; set; }
        protected static string Url { get; set; }
        protected static Credential Credential { get; set; }
        #endregion
    }
}

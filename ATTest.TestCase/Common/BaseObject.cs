﻿using ATTest.TestCase.Settings;
using ATTest.Web;
using Microsoft.VisualStudio.TestPlatform.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ATTest.TestCase.Common
{
    public abstract partial class BaseObject : WebPage
    {
        #region Declare fields

        public string ID { get; set; }

        #endregion

        public BaseObject() : base()
        {
            _urlSettings = ConfigurationService.GetSection<UrlSettings>();
        }
    }
}

using ATTest.TestCase.PageObjects.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;

namespace ATTest.Web.TestCase
{
    [TestClass]
    [TestCategory("Demo")]
    public class LoadPage : MSTest.WebTest
    {
        LoginPage? loginPage;
        CustomerPOM? customePage;

        [TestMethod]
        [Browser(BrowserType.Edge, Lifecycle.RestartOnFail)]
        public void LoginCase()
        {
            loginPage?.SignOn();

            App.Browser.WaitUntilReady();

            //var cookies = App.Cookies.GetAllCookies();
            //foreach (var cookie in cookies)
            //{
            //    Console.WriteLine(cookie.Name + " = " + cookie.Value);
            //}

            customePage?.Open();

            IWebElement a = App.WebElement.FindByClass("fsdfs");

            App.Browser.WaitForUserInteraction(5000);
            Console.WriteLine("Load page complete");
        }

        [TestMethod]
        public void GotoCustomPage()
        {
            Console.WriteLine("Load page complete");

            App.Browser.WaitForAjax();
            var cookies = App.Cookies.GetAllCookies();

            foreach (var cookie in cookies)
            {
                Console.WriteLine(cookie.Name + " = " + cookie.Value);
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            customePage = App.Create<CustomerPOM>();
            loginPage = App.GoTo<LoginPage>();

            App.Browser.WaitForAjax();
            App.Browser.WaitForUserInteraction(5000);

            Console.WriteLine("Initialize completed");
        }
    }
}